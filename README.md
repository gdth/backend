**Documentación del proyecto**

## Generacion de entidades
1. Las entidades se deben crear en app/Domain
2. El nombre debe ser en español y con la primer letra en mayuscula, si es un nombre compuesto debe ser camel case ejemplo: CalculosNomina

---

## Generacion de DTO
1. Los DTO se deben crear en la carpeta app/Services/DTO
2. Ahi se creara una carpeta con el nombre de la entidad en plural, ejemplo: Puestos
3. En la carpeta Puestos crear el interface para el find request, ejemplo: IFindPuestoRequest.php
4. En la carpeta Puestos crear el interface para el request, ejemplo: IPuestoRequest.php
5. En la carpeta Puestos crear el interface para el response, ejemplo: IPuestoResponse.php
6. En la carpeta Puestos crear la clase para el find request, ejemplo: FindPuestoRequest.php
7. En la carpeta Puestos crear la clase para el request, ejemplo: PuestoRequest.php
8. En la carpeta Puestos crear la clase para el response, ejemplo: PuestoResponse.php

---


## Comandos para generar archivos (Query, Repository, Mapper, Services)

1. php artisan make:interfaceQuery INombreQuery (Ubicación: app/DataAccess/Queries/Interfaces)
2. php artisan make:implementQuery NombreQuery (Ubicación: app/DataAccess/Queries/Implement)
3. php artisan make:interfaceRepository INombreRepository (Ubicación: app/DataAccess/Repositories/Interfaces)
4. php artisan make:implementRepository NombreRepository (Ubicación: app/DataAccess/Repositories/Implement)
5. php artisan make:interfaceMapper INombreMapper (Ubicación: app/Services/Mapper/Interfaces)
6. php artisan make:implementMapper NombreMapper (Ubicación: app/Services/Mapper/Implement)
7. php artisan make:interfaceService INombreService (Ubicación: app/Services/Interfaces)
8. php artisan make:implementService NombreService (Ubicación: app/Services/Implement)

Cambiar name por el nombre de la entidad, ejemplo:<br>
*php artisan make:interfaceQuery IRolQuery<br>
php artisan make:implementQuery RolQuery*

## Registrar archivos en AppServiceProvider (Query, Repository, Mapper, Services)
//Nombre
1. $this->app->bind(INombreService::class, NombreService::class);
2. $this->app->bind(INombreQuery::class, NombreQuery::class);
3. $this->app->bind(INombreRepository::class, NombreRepository::class);
4. $this->app->bind(INombreMapper::class, NombreMapper::class);

## Comando para generar Controller
1. php artisan make:controllerApi NameController

## Comandos para generar validator
1. php artisan make:interfaceValidator INombreValidator (Ubicación: app/Services/Validations/Interfaces)
2. php artisan make:implementValidator NombreValidator (Ubicación: app/Services/Validations/Implement)

## Creando politicas
1. php artisan make:policy PostPolicy