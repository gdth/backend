<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    */

    'supportsCredentials' => true,
    'allowedOrigins' => ['*'],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => ['authorization','x-csrf-token','x-requested-with','content-type'],
    'allowedMethods' => ['GET','POST','PUT','DELETE','OPTIONS','PATCH'],
    'exposedHeaders' => [],
    'maxAge' => 0,

];
