<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return redirect("https://pbmg.com.mx/");
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'AuthController@login');

});


Route::group(['middleware' => 'auth:api'], function() {

    Route::get('/logout', 'AuthController@logout');

    Route::group(['prefix' => 'catalogo'], function () {
        Route::group(['prefix' => 'estados'], function () {
            Route::get('/find', 'CEstadoController@find');
            Route::get('/{id}', 'CEstadoController@get');
            Route::post('/', 'CEstadoController@store');
            Route::put('/', 'CEstadoController@update');
            Route::delete('/{id?}', 'CEstadoController@destroy');
        });

        Route::group(['prefix' => 'ciudades'], function () {
            Route::get('/find', 'CCiudadController@find');
            Route::get('/{id}', 'CCiudadController@get');
            Route::post('/', 'CCiudadController@store');
            Route::put('/', 'CCiudadController@update');
            Route::delete('/{id?}', 'CCiudadController@destroy');
        });
    });

    Route::group(['prefix' => 'empresas'], function () {
        Route::get('/find', 'EmpresaController@find');
        Route::get('/{id}', 'EmpresaController@get');
        Route::post('/', 'EmpresaController@store');
        Route::put('/', 'EmpresaController@update');
        Route::delete('/{id?}', 'EmpresaController@destroy');
    });

    Route::group(['prefix' => 'permisos'], function () {
        Route::get('/find', 'PermisoController@find');
        Route::get('/{id}', 'PermisoController@get');
        Route::post('/', 'PermisoController@store');
        Route::put('/', 'PermisoController@update');
        Route::delete('/{id?}', 'PermisoController@destroy');
    });

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/find', 'RolController@find');
        Route::get('/{id}', 'RolController@get');
        Route::post('/', 'RolController@store');
        Route::put('/', 'RolController@update');
        Route::delete('/{id?}', 'RolController@destroy');
    });

    Route::group(['prefix' => 'usuarios'], function () {
        Route::get('/find', 'UsuarioController@find');
        Route::get('/{id}', 'UsuarioController@get');
        Route::post('/', 'UsuarioController@store');
        Route::put('/', 'UsuarioController@update');
        Route::delete('/{id?}', 'UsuarioController@destroy');
    });
});
