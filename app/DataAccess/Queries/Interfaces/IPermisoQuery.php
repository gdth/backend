<?php
namespace App\DataAccess\Queries\Interfaces;

interface IPermisoQuery extends IBaseQuery
{
    public function withName($name);
    public function withDisplayName($displayName);

}
