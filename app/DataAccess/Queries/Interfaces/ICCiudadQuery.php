<?php
namespace App\DataAccess\Queries\Interfaces;

interface ICCiudadQuery extends ICatalogoBaseQuery
{
    public function withEstadoId($estadoId);
    public function includeEstado($includeEstado);

}
