<?php
namespace App\DataAccess\Queries\Interfaces;

interface IUsuarioQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withNombreCompleto($nombreCompleto);
    public function withApellidoPaterno($apellidoPaterno);
    public function withApellidoMaterno($apellidoMaterno);
    public function withTitulo($titulo);
    public function withPuesto($puesto);
    public function withFechaNacimiento($fechaNacimiento);
    public function withMinFechaNacimiento($minFechaNacimiento);
    public function withMaxFechaNacimiento($maxFechaNacimiento);
    public function withTelefono($telefono);
    public function withTelefonoOficina($telefonoOficina);
    public function withExtension($extension);
    public function withCelular($celular);
    public function withEmail($email);
    public function includeEmpresa($includeEmpresa);
    public function withEmpresaId($empresaId);
    public function withActivo($activo);
    public function includeRol($includeRol);
    public function withRolId($rolId);
    public function withRolNombre($rolNombre);
}
