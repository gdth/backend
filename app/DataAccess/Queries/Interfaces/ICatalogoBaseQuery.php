<?php
namespace App\DataAccess\Queries\Interfaces;

interface ICatalogoBaseQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withValor($valor);
    public function withActivo($activo);
}
