<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/31/18
 * Time: 3:44 PM
 */

namespace App\DataAccess\Queries\Interfaces;


interface IBaseQuery
{
    function setCountSelect();
    function init();
    function sort($sortBy, $sort);
    function paginate($paginate, $itemsToShow, $page);
    function totalCount();
    function execute();
}