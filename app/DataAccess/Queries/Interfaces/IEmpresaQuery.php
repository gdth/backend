<?php
namespace App\DataAccess\Queries\Interfaces;

interface IEmpresaQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withCorreo($correo);
    public function withEstadoId($estadoId);
    public function withActivo($activo);
    public function includeEstado($includeEstado);
    public function withCiudadId($ciudadId);
    public function includeCiudad($includeCiudad);

}
