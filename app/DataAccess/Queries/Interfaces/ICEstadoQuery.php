<?php
namespace App\DataAccess\Queries\Interfaces;

interface ICEstadoQuery extends ICatalogoBaseQuery
{
    public function includeCiudades($includeCiudades);
}
