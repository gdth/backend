<?php
namespace App\DataAccess\Queries\Interfaces;

interface IRolQuery extends IBaseQuery
{
    public function withName($name);
    public function withDisplayName($displayName);
    public function withIsDelete($isDelete);
}
