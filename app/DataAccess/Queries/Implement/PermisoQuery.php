<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IPermisoQuery;
use App\Domain\Permiso;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class PermisoQuery implements IPermisoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("permiso")
            ->from(Permiso::class, 'permiso')
            ->where('permiso.id != :id')
            ->setParameter('id', 0);
    }

    public function withName($name)
    {
        if (StringExtensions::isNotNullOrEmpty($name))
            $this->queryable->andWhere('LOWER(permiso.name) LIKE LOWER(:name)')->setParameter('name', '%'.$name.'%');
    }

    public function withDisplayName($displayName)
    {
        if (StringExtensions::isNotNullOrEmpty($displayName))
            $this->queryable->andWhere('LOWER(permiso.display_name) LIKE LOWER(:displayName)')->setParameter('displayName', '%'.$displayName.'%');
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(permiso.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('permiso.'.$sortBy.'', $sort);
    }
}

