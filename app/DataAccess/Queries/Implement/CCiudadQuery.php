<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICCiudadQuery;
use App\Domain\CCiudad;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CCiudadQuery extends CatalogoBaseQuery implements ICCiudadQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CCiudad::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }

    public function withEstadoId($estadoId)
    {
        if (NumberExtensions::isPositiveInteger($estadoId))
            $this->queryable->andWhere('entity.estado = :estadoId')->setParameter('estadoId', $estadoId);
    }

    public function includeEstado($includeEstado)
    {
        if ($includeEstado)
            $this->queryable->leftJoin('entity.estado', 'estado')->addSelect('estado');
    }
}

