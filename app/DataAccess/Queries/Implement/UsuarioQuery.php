<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IUsuarioQuery;
use App\Domain\Usuario;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class UsuarioQuery implements IUsuarioQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("usuario")
            ->from(Usuario::class, 'usuario')
            ->where('usuario.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(usuario.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withNombreCompleto($nombreCompleto)
    {
        if (StringExtensions::isNotNullOrEmpty($nombreCompleto))
            $this->queryable->andWhere("LOWER(CONCAT(usuario.nombre,' ',usuario.apellido_paterno,' ',usuario.apellido_materno)) LIKE LOWER(:nombreCompleto)")->setParameter('nombreCompleto', '%'.$nombreCompleto.'%');
    }

    public function withApellidoPaterno($apellidoPaterno)
    {
        if (StringExtensions::isNotNullOrEmpty($apellidoPaterno))
            $this->queryable->andWhere('LOWER(usuario.apellido_paterno) LIKE LOWER(:apellidoPaterno)')->setParameter('apellidoPaterno', '%'.$apellidoPaterno.'%');
    }

    public function withApellidoMaterno($apellidoMaterno)
    {
        if (StringExtensions::isNotNullOrEmpty($apellidoMaterno))
            $this->queryable->andWhere('LOWER(usuario.apellido_materno) LIKE LOWER(:apellidoMaterno)')->setParameter('apellidoMaterno', '%'.$apellidoMaterno.'%');
    }

    public function withTitulo($titulo)
    {
        if (StringExtensions::isNotNullOrEmpty($titulo))
            $this->queryable->andWhere('LOWER(usuario.titulo) LIKE LOWER(:titulo)')->setParameter('titulo', '%'.$titulo.'%');
    }

    public function withPuesto($puesto)
    {
        if (StringExtensions::isNotNullOrEmpty($puesto))
            $this->queryable->andWhere('LOWER(usuario.puesto) LIKE LOWER(:puesto)')->setParameter('puesto', '%'.$puesto.'%');
    }

    public function withFechaNacimiento($fechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaNacimiento))
            $this->queryable->andWhere('usuario.fecha_nacimiento =:fechaNacimiento')->setParameter('fechaNacimiento', $fechaNacimiento);
    }

    public function withMinFechaNacimiento($minFechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaNacimiento))
            $this->queryable->andWhere('usuario.fecha_nacimiento >=:minFechaNacimiento')->setParameter('minFechaNacimiento', $minFechaNacimiento);
    }

    public function withMaxFechaNacimiento($maxFechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaNacimiento))
            $this->queryable->andWhere('usuario.fecha_nacimiento <=:maxFechaNacimiento')->setParameter('maxFechaNacimiento', $maxFechaNacimiento);
    }

    public function withTelefono($telefono)
    {
        if (StringExtensions::isNotNullOrEmpty($telefono))
            $this->queryable->andWhere('LOWER(usuario.telefono) LIKE LOWER(:telefono)')->setParameter('telefono', '%'.$telefono.'%');
    }

    public function withTelefonoOficina($telefonoOficina)
    {
        if (StringExtensions::isNotNullOrEmpty($telefonoOficina))
            $this->queryable->andWhere('LOWER(usuario.telefono_oficina) LIKE LOWER(:telefonoOficina)')->setParameter('telefonoOficina', '%'.$telefonoOficina.'%');
    }

    public function withExtension($extension)
    {
        if (NumberExtensions::isNumeric($extension))
            $this->queryable->andWhere('usuario.extension = :extension')->setParameter('extension', $extension);
    }

    public function withCelular($celular)
    {
        if (StringExtensions::isNotNullOrEmpty($celular))
            $this->queryable->andWhere('LOWER(usuario.celular) LIKE LOWER(:celular)')->setParameter('celular', '%'.$celular.'%');
    }

    public function withEmail($email)
    {
        if (StringExtensions::isNotNullOrEmpty($email))
            $this->queryable->andWhere('LOWER(usuario.email) LIKE LOWER(:email)')->setParameter('email', '%'.$email.'%');
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa)
            $this->queryable->leftJoin('usuario.empresa', 'empresa')->addSelect('empresa');
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('usuario.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('usuario.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function includeRol($includeRol)
    {
        if ($includeRol)
            $this->queryable->leftJoin('usuario.rol', 'rol')->addSelect('rol');
    }

    public function withRolId($rolId)
    {
        if (NumberExtensions::isPositiveInteger($rolId)) {
            $this->queryable->andWhere('usuario.rol = :rolId')->setParameter('rolId', $rolId);
        }
    }

    public function withRolNombre($rolNombre)
    {
        if(ArrayExtensions::isNotNullOrEmpty($rolNombre)){
            $this->includeRol(true);
            $this->queryable->andWhere('rol.name IN (:rolNombres)')->setParameter('rolNombres', $rolNombre);
        }
    }


    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(usuario.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('usuario.'.$sortBy.'', $sort);
    }
}

