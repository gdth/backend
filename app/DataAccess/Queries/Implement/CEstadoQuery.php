<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICEstadoQuery;
use App\Domain\CEstado;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CEstadoQuery extends CatalogoBaseQuery implements ICEstadoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CEstado::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }

    public function includeCiudades($includeCiudades)
    {
        if ($includeCiudades){
            $this->queryable->leftJoin('entity.ciudades', 'ciudades')->addSelect('ciudades');
            $this->queryable->andWhere('ciudades.activo = :activo_ciudad')->setParameter('activo_ciudad', true);
        }
    }
}

