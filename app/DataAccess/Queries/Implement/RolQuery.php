<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IRolQuery;
use App\Domain\Rol;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class RolQuery implements IRolQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("rol")
            ->from(Rol::class, 'rol')
            ->where('rol.id != :id')
            ->setParameter('id', 0);
    }

    public function withName($name)
    {
        if (StringExtensions::isNotNullOrEmpty($name))
            $this->queryable->andWhere('LOWER(rol.name) LIKE LOWER(:name)')->setParameter('name', '%'.$name.'%');
    }

    public function withDisplayName($displayName)
    {
        if (StringExtensions::isNotNullOrEmpty($displayName))
            $this->queryable->andWhere('LOWER(rol.display_name) LIKE LOWER(:displayName)')->setParameter('displayName', '%'.$displayName.'%');
    }

    public function withIsDelete($isDelete)
    {
        if (StringExtensions::isNotNullOrEmpty($isDelete) ){
            $this->queryable->andWhere('rol.is_delete = :isDelete')->setParameter('isDelete', StringExtensions::toBoolean($isDelete));
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(rol.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('rol.'.$sortBy.'', $sort);
    }
}

