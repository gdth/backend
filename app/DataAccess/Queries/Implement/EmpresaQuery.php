<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IEmpresaQuery;
use App\Domain\Empresa;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class EmpresaQuery implements IEmpresaQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("empresa")
            ->from(Empresa::class, 'empresa')
            ->where('empresa.id != :id')
            ->setParameter('id', 0);
    }
    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(empresa.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withCorreo($correo)
    {
        if (StringExtensions::isNotNullOrEmpty($correo))
            $this->queryable->andWhere('LOWER(empresa.correo) LIKE LOWER(:correo)')->setParameter('correo', '%'.$correo.'%');
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('empresa.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withEstadoId($estadoId)
    {
        if (NumberExtensions::isPositiveInteger($estadoId))
            $this->queryable->andWhere('empresa.estado = :estadoId')->setParameter('estadoId', $estadoId);
    }

    public function includeEstado($includeEstado)
    {
        if ($includeEstado)
            $this->queryable->leftJoin('empresa.estado', 'ciudad')->addSelect('ciudad');
    }

    public function withCiudadId($ciudadId)
    {
        if (NumberExtensions::isPositiveInteger($ciudadId))
            $this->queryable->andWhere('empresa.ciudad = :ciudadId')->setParameter('ciudadId', $ciudadId);
    }

    public function includeCiudad($includeCiudad)
    {
        if ($includeCiudad)
            $this->queryable->leftJoin('empresa.ciudad', 'ciudad')->addSelect('ciudad');
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(empresa.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('empresa.'.$sortBy.'', $sort);
    }
}

