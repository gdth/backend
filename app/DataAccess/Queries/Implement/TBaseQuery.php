<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\Infrastructure\NumberExtensions;
use function camel_case;
use Doctrine\ORM\Query;
use function get_class;
use function str_replace;

trait TBaseQuery
{
    protected $unitOfWork;
    protected $queryable;
    protected $hydrationMode;
    protected $domainClassName;

    public function __construct(IUnitOfWork $unitOfWork)
    {
        $this->unitOfWork = $unitOfWork;
        $this->queryable = $this->unitOfWork->Queryable();
        $this->hydrationMode = Query::HYDRATE_OBJECT;
        $this->domainClassName = str_replace("Query", "", camel_case(class_basename(get_class()))) ;
    }

    function paginate($paginate, $itemsToShow, $page)
    {
        if($paginate && NumberExtensions::isPositiveInteger($itemsToShow) && NumberExtensions::isPositiveInteger($page)){
            $this->queryable->setFirstResult($itemsToShow * ($page -1))->setMaxResults($itemsToShow);
        }
    }

    function totalCount() {
        $total_count = $this->queryable->getQuery()->getResult();
        return count($total_count);
    }

    function execute($hydrationMode = null)
    {
        $this->hydrationMode = $hydrationMode ?? $this->hydrationMode;
        return  $this->queryable->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getResult($this->hydrationMode);
    }
}