<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICatalogoBaseQuery;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CatalogoBaseQuery implements ICatalogoBaseQuery
{
    use TBaseQuery;

    function init()
    {
    }

    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(entity.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withValor($valor)
    {
        if (StringExtensions::isNotNullOrEmpty($valor))
            $this->queryable->andWhere('entity.valor = :valor')->setParameter('valor', $valor);
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('entity.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(entity.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('entity.'.$sortBy.'', $sort);
    }
}

