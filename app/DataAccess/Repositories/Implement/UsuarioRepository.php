<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Domain\Usuario;

class UsuarioRepository implements IUsuarioRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('usuario')
            ->from(Usuario::class, 'usuario')
            ->where('usuario.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}