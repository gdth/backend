<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\ICEstadoRepository;
use App\Domain\CEstado;

class CEstadoRepository implements ICEstadoRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('estado')
            ->from(CEstado::class, 'estado')
            ->where('estado.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}