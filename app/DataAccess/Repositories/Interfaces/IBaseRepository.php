<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/31/18
 * Time: 3:38 PM
 */

namespace App\DataAccess\Repositories\Interfaces;


use App\DataAccess\Configs\IUnitOfWork;

interface IBaseRepository
{
    /**
     * @return IUnitOfWork
     */
    public function getUnitOfWork();
    public function get($id);
    public function add($entity);
    public function update($entity);
    public function remove($entity);
}