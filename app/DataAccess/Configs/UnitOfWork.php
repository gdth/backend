<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/10/18
 * Time: 3:00 PM
 */

namespace App\DataAccess\Configs;


use Doctrine\ORM\EntityManager;

class UnitOfWork implements IUnitOfWork
{
    protected $entityManager;
    protected $autoCommit;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->autoCommit = true;
    }

    public function beginTransaction()
    {
        $this->autoCommit = false;
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit($this->autoCommit);
    }

    public function commit()
    {
        $this->entityManager->flush();
        $this->entityManager->commit();
        $this->entityManager->close();
    }

    public function rollback()
    {
        $this->entityManager->rollBack();
        $this->entityManager->close();
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function insert($entity)
    {
        $this->entityManager->persist($entity);
        $this->flush();
    }

    public function update($entity)
    {
        $this->entityManager->merge($entity);
        $this->flush();
    }

    public function remove($entity)
    {
        $this->entityManager->remove($entity);
        $this->flush();
    }

    public function queryable()
    {
        return $this->entityManager->createQueryBuilder();
    }

    private function flush(){
        if($this->autoCommit){
            $this->entityManager->flush();
        }
    }
}