<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IRolService;
use App\Services\Validations\Interfaces\IRolValidator;
use Illuminate\Http\Request;

class RolController extends Controller
{
    protected $rolService;
    protected $rolValidator;

    public function __construct(
        IRolService $rolService,
        IRolValidator $rolValidator
    )
    {
        $this->rolService = $rolService;
        $this->rolValidator = $rolValidator;
    }

    public function find(Request $request)
    {
        $this->rolValidator->validateFind($request);
        return $this->rolService->find($request);
    }

    public function get($id)
    {
        $this->rolValidator->validateGet($id);
        return $this->rolService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->rolValidator->validateStore($request);
        return $this->rolService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->rolValidator->validateUpdate($request);
        return $this->rolService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->rolValidator->validateDelete($id);
        return $this->rolService->delete($id);
    }
}