<?php
namespace App\Http\Controllers;

use App\Domain\Usuario;
use App\Services\Interfaces\IUsuarioService;
use App\Services\Validations\Interfaces\IUsuarioValidator;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    protected $usuarioService;
    protected $usuarioValidator;

    public function __construct(
        IUsuarioService $usuarioService,
        IUsuarioValidator $usuarioValidator
    )
    {
        $this->usuarioService = $usuarioService;
        $this->usuarioValidator = $usuarioValidator;
    }

    public function find(Request $request)
    {
        $this->authorize('findAuthorize',  [Usuario::class, $request]);
        $this->usuarioValidator->validateFind($request);
        return $this->usuarioService->find($request);
    }

    public function get($id)
    {
        $this->authorize('getAuthorize',  [Usuario::class, $id]);
        $this->usuarioValidator->validateGet($id);
        return $this->usuarioService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->authorize('storeAuthorize',  [Usuario::class, $request]);
        $this->usuarioValidator->validateStore($request);
        return $this->usuarioService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->authorize('updateAuthorize',  [Usuario::class, $request]);
        $this->usuarioValidator->validateUpdate($request);
        return $this->usuarioService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->authorize('destroyAuthorize',  [Usuario::class, $id]);
        $this->usuarioValidator->validateDelete($id);
        return $this->usuarioService->delete($id);
    }
}