<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IEmpresaService;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{

    protected $empresaService;

    public function __construct(IEmpresaService $empresaService)
    {
        $this->empresaService = $empresaService;
    }

    public function find(Request $request)
    {
        return $this->empresaService->find($request);
    }

    public function get($id)
    {
        return $this->empresaService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->empresaService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        return $this->empresaService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->empresaService->delete($id);
    }
}