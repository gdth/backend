<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IAuthService;
use App\Services\Validations\Interfaces\IAuthValidator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authValidator;
    protected $authService;

    public function __construct(IAuthValidator $authValidator, IAuthService $authService)
    {
        $this->authValidator = $authValidator;
        $this->authService = $authService;
    }

    public function login(Request $request){

        $this->authValidator->validateLogin($request);
        return $this->authService->login($request);
    }

    public function logout(Request $request){
        return $this->authService->logout($request);
    }
}