<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICCiudadService;
use Illuminate\Http\Request;

class CCiudadController extends Controller
{
    protected $ciudadService;
    public function __construct(ICCiudadService $ciudadService)
    {
        $this->ciudadService = $ciudadService;
    }

    public function find(Request $request)
    {
        return $this->ciudadService->find($request);
    }

    public function get($id)
    {
        return $this->ciudadService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->ciudadService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        return $this->ciudadService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->ciudadService->delete($id);
    }
}