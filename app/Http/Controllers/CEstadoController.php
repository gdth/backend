<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICEstadoService;
use Illuminate\Http\Request;

class CEstadoController extends Controller
{
    protected $estadoService;

    public function __construct(ICEstadoService $estadoService)
    {
        $this->estadoService = $estadoService;
    }

    public function find(Request $request)
    {
        return $this->estadoService->find($request);
    }

    public function get($id)
    {
        return $this->estadoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->estadoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        return $this->estadoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->estadoService->delete($id);
    }
}