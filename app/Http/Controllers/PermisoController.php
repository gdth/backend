<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IPermisoService;
use Illuminate\Http\Request;

class PermisoController extends Controller
{
    protected $permisoService;

    public function __construct(IPermisoService $permisoService)
    {
        $this->permisoService = $permisoService;
    }

    public function find(Request $request)
    {
        return $this->permisoService->find($request);
    }

    public function get($id)
    {
        return $this->permisoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->permisoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        return $this->permisoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->permisoService->delete($id);
    }
}