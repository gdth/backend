<?php

namespace App\Console;
use App\Console\Commands\controllerApiCommand;
use App\Console\Commands\mapperImpCommand;
use App\Console\Commands\mapperIntCommand;
use App\Console\Commands\queryCommand;
use App\Console\Commands\queryImpCommand;
use App\Console\Commands\queryIntCommand;
use App\Console\Commands\repositoryImpCommand;
use App\Console\Commands\repositoryIntCommand;
use App\Console\Commands\serviceImpCommand;
use App\Console\Commands\serviceIntCommand;
use App\Console\Commands\validatorImpCommand;
use App\Console\Commands\validatorIntCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        queryIntCommand::class,
        queryImpCommand::class,
        repositoryIntCommand::class,
        repositoryImpCommand::class,
        mapperIntCommand::class,
        mapperImpCommand::class,
        serviceIntCommand::class,
        serviceImpCommand::class,
        controllerApiCommand::class,
        validatorIntCommand::class,
        validatorImpCommand::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
