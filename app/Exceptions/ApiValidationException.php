<?php

namespace App\Exceptions;

use App\Services\Validations\StatusCode;
use Exception;

class ApiValidationException extends Exception
{
    /**
     * The status code to use for the response.
     *
     * @var int
     */
    public $status = StatusCode::HTTP_UNPROCESSABLE_ENTITY;

    public $errors = [];
    /**
     * ApiValidationException constructor.
     */
    public function __construct($errors)
    {

        parent::__construct('The given data was invalid.', $this->status);
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

}
