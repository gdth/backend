<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/31/18
 * Time: 3:55 PM
 */

namespace App\Infrastructure;


class StringExtensions
{


    public static function toBoolean($stringValue)
    {
        return 'true' == $stringValue;
    }

    public static function isNull($stringValue)
    {
        return $stringValue == 'null' || $stringValue == null;
    }

    public static function isNotNull($stringValue)
    {
        $isNull = $stringValue == 'null' || $stringValue == null;
        return !$isNull;
    }

    public static function isNotNullString($stringValue)
    {
        $stringValue = strtolower($stringValue);

        $isNotNullString =
            $stringValue == 'notnull'  || $stringValue == 'nonull'  ||
            $stringValue == 'not-null' || $stringValue == 'no-null' ||
            $stringValue == 'not_null' || $stringValue == 'no_null' ||
            $stringValue == 'not null' || $stringValue == 'no null';
        return $isNotNullString;
    }

    public static function isEmpty($stringValue)
    {
        return $stringValue == '';
    }

    public static function isNotEmpty($stringValue)
    {
        $isNullOrEmpty = $stringValue == '';
        return !$isNullOrEmpty;
    }

    public static function isNullOrEmpty($stringValue)
    {
        return $stringValue == '' || $stringValue == null;
    }

    public static function isNotNullOrEmpty($stringValue)
    {
        $isNullOrEmpty = $stringValue == '' || $stringValue == null;
        return !$isNullOrEmpty;
    }

    public static function toInt($stringValue)
    {
        return (int) $stringValue;
    }

    public static function toDecimal($stringValue)
    {
        return (float) $stringValue;
    }

    public static function placeholder($content , array $values){
        $_values = array();

        foreach ($values as $key => $value) {
            $_values['{' . $key . '}'] = $value;
        }
        return strtr($content, $_values);
    }

    public static function sanitize($string){
        return  preg_replace('/\&(.)[^;]*;/', '\\1', htmlentities($string));
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function splitCamelCaseToWords($camelCaseString) {
        $re = '/(?<=[a-z])(?=[A-Z])/x';
        $a = preg_split($re, $camelCaseString);
        return join($a, " " );
    }
}