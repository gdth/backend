<?php
/**
 * Created by PhpStorm.
 * Usuario: karimy chable
 * Date: 13/02/2018
 * Time: 10:20 AM
 */

namespace App\Infrastructure;


class BooleanExtensions
{

    public static function isBoolean($booleanValue)
    {
        return (is_bool($booleanValue) ||
            in_array($booleanValue,
                array("true", "false"), true));
    }

}