<?php
namespace App\Infrastructure;
use Doctrine\ORM\QueryBuilder;

class QueryExtensions
{
    /**
     * @param $aliases
     * @param $queryBuilder QueryBuilder
     * @return bool
     */
    public static function isInQuery($aliases, $queryBuilder){
        return ArrayExtensions::search($aliases, $queryBuilder->getAllAliases());

    }
    public static function isNotInQuery($aliases, $queryBuilder){
        return !self::isInQuery($aliases, $queryBuilder);
    }
}