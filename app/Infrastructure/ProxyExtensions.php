<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 13/02/2018
 * Time: 10:20 AM
 */

namespace App\Infrastructure;


use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\PersistentCollection;
use Doctrine\ORM\Proxy\Proxy;

class ProxyExtensions
{

    public static function load($entity) {
        if ($entity != null && $entity instanceof Proxy)
            $entity->__load();
    }

    public static function toArray($collection) {
        if ($collection != null && $collection instanceof Collection)
            $collection->toArray();
    }

    public static function initialize($entity){
        if(self::isProxy($entity)){
            self::load($entity);
        }
        elseif (self::isCollection($entity)){
            self::toArray($entity);
        }
    }

    public static function isInitialized($entity, $includeEntity = false){
        /* @var $entity Proxy | PersistentCollection */

        return
            $entity != null && (
            (self::isProxy($entity) && $entity->__isInitialized())
                ||
            (self::isCollection($entity) && $entity->isInitialized())
                || ($includeEntity && self::isNotProxy($entity))
        );
    }

    public static function isProxy($entity){
        return $entity instanceof Proxy;
    }

    public static function isNotProxy($entity){
        return !self::isProxy($entity);
    }

    public static function isEntity($entity){
        return self::isNotProxy($entity) && !self::isCollection($entity);
    }

    public static function isCollection($entity){
        return $entity instanceof Collection;
    }

}