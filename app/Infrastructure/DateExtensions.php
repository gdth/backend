<?php
/**
 * Created by PhpStorm.
 * Usuario: karimy chable
 * Date: 13/02/2018
 * Time: 10:20 AM
 */

namespace App\Infrastructure;

use function is_string;

class DateExtensions
{
    public static function getDateFormatted($date, $format = 'Y-m-d')
    {

        $dateFormatted = null;

        if (StringExtensions::isNotNullOrEmpty($date)) {
            if (is_string($date)) {
                $date = new \DateTime($date);
            }
            if ($date instanceof \DateTime) {
                $dateFormatted = $date->format($format);
            }
        }

        return $dateFormatted;

    }

    public static function getTimeFormatted($time, $format = 'H:i:s')
    {

        $timeFormatted = null;

        if (StringExtensions::isNotNullOrEmpty($time)) {
            if (is_string($time)) {
                $time = new \DateTime($time);
            }
            if ($time instanceof \DateTime) {
                $timeFormatted = $time->format($format);
            }
        }

        return $timeFormatted;

    }

    public static function getStringToTimeFormatted($time)
    {
        $timeFormatted = null;
        if (StringExtensions::isNotNullOrEmpty($time)) {
            if (is_string($time)) {
                $timeFormatted = new \DateTime($time);
            }
        }
        return $timeFormatted;

    }

    public static function getDaysByMonth($month)
    {
        $year = date("Y");
        $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        return $days;
    }
}