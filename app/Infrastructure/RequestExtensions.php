<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 12/09/18
 * Time: 15:28
 */

namespace App\Infrastructure;


class RequestExtensions
{
    public static function isForCreate($request)
    {
        return (!isset($request['id']) || !$request['id'] > 0);
    }

    public static function isForUpdate($request)
    {
        return (isset($request['id']) && $request['id'] > 0);
    }
}