<?php


namespace App\Infrastructure;


use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Configs\UnitOfWork;
use App\DataAccess\Repositories\Implement\PlantillaPropuestaRepository;
use App\Domain\Atributo;
use App\Domain\AtributoElementoPagina;
use App\Domain\ElementoPagina;
use App\Domain\ETipoPagina;
use App\Domain\GrupoPagina;
use App\Domain\Pagina;
use App\Domain\PlantillaPropuesta;
use App\Domain\TipoElemento;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use function resolve;

class SeederExtensions
{
    public static function seedTemplates($info){

        /* @var $unitOfWork UnitOfWork
        *   @var $plantilla PlantillaPropuesta
         *  @var $eTipoPagina ETipoPagina
         */
        $unitOfWork = resolve(IUnitOfWork::class);
        $plantillaPropuestaRepository = resolve(PlantillaPropuestaRepository::class);


        $plantilla = $unitOfWork->getEntityManager()->getRepository(PlantillaPropuesta::class)->find(1);

        $infoPagina = $info["pagina"];

        $eTipoPagina = $unitOfWork->getEntityManager()->getRepository(ETipoPagina::class)->findOneBy(["valor"=>$infoPagina["e_tipo_pagina_id"]]);

        $pagina = new Pagina();
        $pagina->setNombre($infoPagina["nombre"]);
        $pagina->setDescripcion($infoPagina["descripcion"]);
        $pagina->setDirectorio($infoPagina["directorio"]);
        $pagina->setActivable($infoPagina["activable"]);
        $pagina->setActivo(true);
        $pagina->setEliminado(false);
        $pagina->setETipoPagina($eTipoPagina);
        $pagina->setOrden($infoPagina["orden"]);
        $pagina->setMostrarOpciones($infoPagina["mostrar_opciones"]);

        $grupos = $info["grupos"];

        foreach ($grupos as $indice => $grupo){
            $grupoPagina = new GrupoPagina();
            $grupoPagina->setNombre($grupo["nombre"]);
            $grupoPagina->setOrden($grupo["orden"]);
            $grupoPagina->setActivable($grupo["activable"]);

            if(array_get($grupo, "activable")){
                $grupoPagina->setActivo($grupo["activo"]);
            }

            /* @var $tipoElementos ArrayCollection*/
            $tipoElementos = new ArrayCollection($unitOfWork->getEntityManager()->getRepository(TipoElemento::class)->findAll());

            foreach ($grupo["elementos"] as $indiceElemento => $elemento) {

                $tipoElemento = $tipoElementos->matching(Criteria::create()->where((Criteria::expr()->eq("id", $elemento["tipo_elemento_id"]))))->first();

                $elementoPagina = new ElementoPagina();
                $elementoPagina->setNombre($elemento["nombre"]);
                $elementoPagina->setOrden($elemento["orden"]);
                $elementoPagina->setTipoElemento($tipoElemento);


                foreach ($elemento["atributos"] as $indiceAtributo=> $atributo){
                    $atributoElementoPagina = new AtributoElementoPagina();
                    $atributoBd = $unitOfWork->getEntityManager()->getRepository(Atributo::class)->find($atributo["id"]);
                    $atributoElementoPagina->setAtributo($atributoBd);
                    $atributoElementoPagina->setValor($atributo["valor"]);
                    $elementoPagina->addAtributoElementoPagina($atributoElementoPagina);
                }

                $grupoPagina->addElementoPagina($elementoPagina);
            }

            $pagina->addGrupoPagina($grupoPagina);
        }

        $plantilla->addPagina($pagina);

        $plantillaPropuestaRepository->update($plantilla);
    }

}