<?php


namespace App\Infrastructure;


use Exception;
use function Functional\concat;
use Illuminate\Support\Facades\File;

class FileExtensions
{
    public static function store($archivo, $directorio, $nombreArchivo){
        $success = false;

        try{
            if(!File::isDirectory($directorio)){
                File::makeDirectory($directorio, 0777, true);
            }
            $success = File::put(concat($directorio,$nombreArchivo), file_get_contents($archivo))!==false;
        }
        catch(Exception $exception){

        }

        return $success;
    }
    public static function delete($archivo){
        $success = false;

        if(File::isFile($archivo) && File::exists($archivo)){
            $success = File::delete($archivo);
        }

        return $success;
    }
    public static function generateRandomName($nombre, $extension){
        return concat(str_slug($nombre, "-"),"-",time(), ".", $extension);;
    }
}