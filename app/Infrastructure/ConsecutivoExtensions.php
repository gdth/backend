<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 15/08/18
 * Time: 13:05
 */

namespace App\Infrastructure;

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\Consecutivo;
use App\Domain\Enum;
use App\Domain\ETipoConsecutivo;
use App\Domain\Sucursal;

class ConsecutivoExtensions
{
    /* @var $unitOfWork IUnitOfWork*/
    protected static $unitOfWork;

    private static function init()
    {
        self::$unitOfWork = resolve(IUnitOfWork::class);
    }

    public static function getLastConsecutive($sucursalId, $eTipoConsecutivoValor)
    {
        self::init();
        $consecutivo = self::getConsecutive($sucursalId, $eTipoConsecutivoValor);
        return self::formatConsecutive($consecutivo->getFolio(), $eTipoConsecutivoValor);
    }

    public static function getNextConsecutive($sucursalId, $eTipoConsecutivoValor)
    {
        self::init();
        $consecutivo = self::getConsecutive($sucursalId, $eTipoConsecutivoValor);
        return self::formatConsecutive($consecutivo->getFolio() + 1, $eTipoConsecutivoValor);
    }

    public static function updateConsecutive($sucursalId, $eTipoConsecutivoValor)
    {
        self::init();
        $consecutivo = self::getConsecutive($sucursalId, $eTipoConsecutivoValor);
        $folio = $consecutivo->getFolio() + 1;
        $consecutivo->setFolio($folio);
        self::$unitOfWork->update($consecutivo);
    }

    /**
     * @param $sucursalId
     * @param $eTipoConsecutivoValor
     * @return Consecutivo
     */
    private static function getConsecutive($sucursalId, $eTipoConsecutivoValor)
    {
        /** @var ETipoConsecutivo $eTipoConsecutivo */
        $eTipoConsecutivo = self::$unitOfWork
            ->getEntityManager()
            ->getRepository(Enum::class)
            ->findOneBy(["valor" => $eTipoConsecutivoValor]);

        /** @var Consecutivo $consecutivo */
        $consecutivo = self::$unitOfWork
            ->getEntityManager()
            ->getRepository(Consecutivo::class)
            ->findOneBy(array('sucursal' => $sucursalId, 'e_tipo_consecutivo' => $eTipoConsecutivo->getId()));

        if ($consecutivo == null) {
            /** @var Sucursal $sucursal */
            $sucursal = self::$unitOfWork
                ->getEntityManager()
                ->getRepository(Sucursal::class)
                ->find($sucursalId);

            $consecutivo = self::createConsecutive($sucursal, $eTipoConsecutivo);
        }

        return $consecutivo;
    }

    /**
     * @param Sucursal $sucursal
     * @param ETipoConsecutivo $eTipoConsecutivo
     * @return Consecutivo
     */
    private static function createConsecutive($sucursal, $eTipoConsecutivo)
    {
        $consecutivo = new Consecutivo();
        $consecutivo->setSucursal($sucursal);
        $consecutivo->setETipoConsecutivo($eTipoConsecutivo);
        $consecutivo->setFolio(0);

        self::$unitOfWork
            ->insert($consecutivo);

        return $consecutivo;
    }

    /**
     * @param $consecutivo
     * @param $eTipoConsecutivoValor
     * @return string
     */
    private static function formatConsecutive($consecutivo, $eTipoConsecutivoValor)
    {
        $folio = str_pad($consecutivo, 5, "0", STR_PAD_LEFT);

        switch ($eTipoConsecutivoValor) {
            case ETipoConsecutivo::COTIZACION:
                $folio = "C" . $folio;
                break;
            case ETipoConsecutivo::PROPUESTA:
                $folio = "P" . $folio;
                break;
        }

        return $folio;
    }
}