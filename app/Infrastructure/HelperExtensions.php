<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 1/28/19
 * Time: 1:09 PM
 */

namespace App\Infrastructure;


class HelperExtensions
{

    public static function setGenre($sex){
        $sexo = 'Masculino';
        if($sex == 'F'){
            $sexo = 'Femenino';
        }
        return $sexo;
    }

    public static function formatDate($date){
        $fechaFormat = '01/01/2019';
        $fecha = explode('-', $date);
        $fechaFormat = $fecha[2].'/'.$fecha[1].'/'.$fecha[0];
        return $fechaFormat;
    }
}