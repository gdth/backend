<?php


namespace App\Infrastructure;


class CryptExtensions
{
    public static function encrypt($string = null){
        $secretKey= config("app.crypt.secret_key");
        $secretIv = config("app.crypt.secret_iv");
        $cipherAlgorithm = config("app.crypt.cipher_algorithm");
        $hashedSecretKey= self::hashSecretKey($secretKey);
        $hashedSecretIv=self::hashSecretIv($secretIv);
        return openssl_encrypt($string, $cipherAlgorithm , $hashedSecretKey, 0, $hashedSecretIv);
    }

    public static function decrypt($string = null){
        $secretKey= config("app.crypt.secret_key");
        $secretIv = config("app.crypt.secret_iv");
        $cipherAlgorithm = config("app.crypt.cipher_algorithm");
        $hashedSecretKey= self::hashSecretKey($secretKey);
        $hashedSecretIv=self::hashSecretIv($secretIv);
        return openssl_decrypt($string, $cipherAlgorithm, $hashedSecretKey, 0, $hashedSecretIv);
    }

    private static function hashSecretKey($secretIv){
        $hashingAlgorithm = config("app.crypt.hashing_algorithm");
        return hash($hashingAlgorithm, $secretIv);
    }
    private static function hashSecretIv($secretIv){
        $hashingAlgorithm = config("app.crypt.hashing_algorithm");
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        return substr(hash($hashingAlgorithm, $secretIv), 0, 16);
    }
}