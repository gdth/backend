<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IEmpresaQuery;
use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\Domain\Empresa;
use App\Services\DTO\Base\CreateResponse;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Empresas\EmpresaRequest;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Empresas\FindEmpresasRequest;
use App\Services\Interfaces\IEmpresaService;
use App\Services\Mapper\Interfaces\IEmpresaMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;


class EmpresaService implements IEmpresaService
{
    protected $unitOfWork;
    protected $empresaRepository;
    protected $empresaQuery;
    protected $empresaMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IEmpresaRepository $empresaRepository,
        IEmpresaQuery $empresaQuery,
        IEmpresaMapper $empresaMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->empresaRepository    = $empresaRepository;
        $this->empresaQuery         = $empresaQuery;
        $this->empresaMapper        = $empresaMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findEmpresaRequest = new FindEmpresasRequest($request->all());

        //Query
        $this->empresaQuery->init();
        $this->empresaQuery->withNombre($findEmpresaRequest->getNombre());
        $this->empresaQuery->withCorreo($findEmpresaRequest->getCorreo());
        $this->empresaQuery->withActivo($findEmpresaRequest->getActivo());
        $this->empresaQuery->withEstadoId($findEmpresaRequest->getEstadoId());
        $this->empresaQuery->includeEstado($findEmpresaRequest->isIncludeEstado());
        $this->empresaQuery->withCiudadId($findEmpresaRequest->getCiudadId());
        $this->empresaQuery->includeCiudad($findEmpresaRequest->isIncludeCiudad());
        //Orden
        $this->empresaQuery->sort($findEmpresaRequest->getSortBy(), $findEmpresaRequest->getSort());
        //Total registros
        $totalCount = $this->empresaQuery->totalCount();

        //Paginacion
        $this->empresaQuery->paginate(
            $findEmpresaRequest->getPaginate(),
            $findEmpresaRequest->getItemsToShow(),
            $findEmpresaRequest->getPage()
        );

        //Ejecutar query
        $empresas = $this->empresaQuery->execute();

        /*dd($empresas);*/

        //Mapear query
        $empresasResponse = $this->empresaMapper->getMapper()->mapMultiple($empresas, EmpresaResponse::class);

        //Find data response
        $findEmpresasDataResponse = new FindDataResponse();
        $findEmpresasDataResponse->setResults($empresasResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findEmpresaRequest->getPage());
        $paginationResponse->setPageCount(count($empresasResponse));
        $paginationResponse->setPageSize($findEmpresaRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findEmpresasResponse = new  FindResponse();
        $findEmpresasResponse->setData($findEmpresasDataResponse);
        $findEmpresasResponse->setPagination($paginationResponse);

        return response()->json($findEmpresasResponse);

    }

    public function get($id)
    {
        /* @var $empresa Empresa*/
        $empresa = $this->empresaRepository->get($id);
        $empresaResponse = $this->empresaMapper->getMapper()->map($empresa, EmpresaResponse::class);
        return response()->json($empresaResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $empresa Empresa*/
        $empresaRequest = new EmpresaRequest($request->all());
        $empresa = $this->empresaMapper->getMapper()
            ->map($empresaRequest, Empresa::class);
        $empresa->setActivo(true);
        $this->empresaRepository->add($empresa);
        $createResponse = new CreateResponse();
        $createResponse->setId($empresa->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $empresa Empresa*/
            $empresaRequest = new EmpresaRequest($request->all());
            $empresa = $this->empresaRepository->get($empresaRequest->getId());
            $this->empresaMapper->getMapper()->mapToObject($empresaRequest, $empresa);
            $this->empresaRepository->update($empresa);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $empresa Empresa*/

        //Eliminado logico
        try{
            $empresa = $this->empresaRepository->get($id);
            $empresa->setActivo(false);
            $this->empresaRepository->update($empresa);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}
