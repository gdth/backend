<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICEstadoQuery;
use App\DataAccess\Repositories\Interfaces\ICEstadoRepository;
use App\Domain\CEstado;
use App\Services\DTO\Base\CreateResponse;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\CEstados\CEstadoRequest;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\DTO\CEstados\FindCEstadosRequest;
use App\Services\Interfaces\ICEstadoService;
use App\Services\Mapper\Interfaces\ICEstadoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;


class CEstadoService implements ICEstadoService
{
    protected $unitOfWork;
    protected $estadoRepository;
    protected $estadoQuery;
    protected $estadoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICEstadoRepository $estadoRepository,
        ICEstadoQuery $estadoQuery,
        ICEstadoMapper $estadoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->estadoRepository = $estadoRepository;
        $this->estadoQuery      = $estadoQuery;
        $this->estadoMapper     = $estadoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findEstadoRequest = new FindCEstadosRequest($request->all());
        //Query
        $this->estadoQuery->init();
        $this->estadoQuery->withNombre($findEstadoRequest->getNombre());
        $this->estadoQuery->withActivo($findEstadoRequest->getActivo());
        $this->estadoQuery->includeCiudades($findEstadoRequest->isIncludeCiudades());

        //Orden
        $this->estadoQuery->sort($findEstadoRequest->getSortBy(), $findEstadoRequest->getSort());

        $totalCount = $this->estadoQuery->totalCount();

        //Paginacion
        $this->estadoQuery->paginate(
            $findEstadoRequest->getPaginate(),
            $findEstadoRequest->getItemsToShow(),
            $findEstadoRequest->getPage()
        );

        //Ejecutar query
        $estados = $this->estadoQuery->execute();

        //Mapear query
        $estadosResponse = $this->estadoMapper->getMapper()->mapMultiple($estados, CEstadoResponse::class);

        //Find data response
        $findEstadosDataResponse = new FindDataResponse();
        $findEstadosDataResponse->setResults($estadosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findEstadoRequest->getPage());
        $paginationResponse->setPageCount(count($estadosResponse));
        $paginationResponse->setPageSize($findEstadoRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findEstadosResponse = new  FindResponse();
        $findEstadosResponse->setData($findEstadosDataResponse);
        $findEstadosResponse->setPagination($paginationResponse);

        return response()->json($findEstadosResponse);

    }

    public function get($id)
    {
        /* @var $estado CEstado*/
        $estado = $this->estadoRepository->get($id);
        $estadoResponse = $this->estadoMapper->getMapper()
            ->map($estado, CEstadoResponse::class);
        return response()->json($estadoResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $estado CEstado*/
        $estadoRequest = new CEstadoRequest($request->all());
        $estado = $this->estadoMapper->getMapper()->map($estadoRequest, CEstado::class);
        $estado->setActivo(true);
        $this->estadoRepository->add($estado);

        $createResponse = new CreateResponse();
        $createResponse->setId($estado->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $estado CEstado*/
            $estadoRequest = new CEstadoRequest($request->all());
            $estado = $this->estadoRepository->get($estadoRequest->getId());
            $this->estadoMapper->getMapper()->mapToObject($estadoRequest, $estado);
            $this->estadoRepository->update($estado);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $estado CEstado*/

        //Eliminado logico
        try{
            $estado = $this->estadoRepository->get($id);
            $estado->setActivo(false);

            $this->estadoRepository->update($estado);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}
