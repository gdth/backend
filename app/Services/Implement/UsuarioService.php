<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IUsuarioQuery;
use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Domain\Usuario;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Usuarios\FindUsuariosRequest;
use App\Services\DTO\Usuarios\UsuarioRequest;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Interfaces\IUsuarioService;
use App\Services\Mapper\Interfaces\IUsuarioMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class UsuarioService implements IUsuarioService
{
    protected $unitOfWork;
    protected $usuarioRepository;
    protected $usuarioQuery;
    protected $usuarioMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IUsuarioRepository $usuarioRepository,
        IUsuarioQuery $usuarioQuery,
        IUsuarioMapper $usuarioMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->usuarioRepository    = $usuarioRepository;
        $this->usuarioQuery         = $usuarioQuery;
        $this->usuarioMapper        = $usuarioMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findUsuarioRequest = new FindUsuariosRequest($request->all());
        //Query
        $this->usuarioQuery->init();
        $this->usuarioQuery->withRolNombre($findUsuarioRequest->getRolNombre());
        $this->usuarioQuery->withNombre($findUsuarioRequest->getNombre());
        $this->usuarioQuery->withNombreCompleto($findUsuarioRequest->getNombreCompleto());
        $this->usuarioQuery->withApellidoPaterno($findUsuarioRequest->getApellidoPaterno());
        $this->usuarioQuery->withApellidoMaterno($findUsuarioRequest->getApellidoMaterno());
        $this->usuarioQuery->withTitulo($findUsuarioRequest->getTitulo());
        $this->usuarioQuery->withPuesto($findUsuarioRequest->getPuesto());
        $this->usuarioQuery->withFechaNacimiento($findUsuarioRequest->getFechaNacimiento());
        $this->usuarioQuery->withMinFechaNacimiento($findUsuarioRequest->getMinFechaNacimiento());
        $this->usuarioQuery->withMaxFechaNacimiento($findUsuarioRequest->getMaxFechaNacimiento());
        $this->usuarioQuery->withTelefono($findUsuarioRequest->getTelefono());
        $this->usuarioQuery->withTelefonoOficina($findUsuarioRequest->getTelefonoOficina());
        $this->usuarioQuery->withExtension($findUsuarioRequest->getExtension());
        $this->usuarioQuery->withCelular($findUsuarioRequest->getCelular());
        $this->usuarioQuery->withEmail($findUsuarioRequest->getEmail());
        $this->usuarioQuery->includeEmpresa($findUsuarioRequest->isIncludeEmpresa());
        $this->usuarioQuery->withEmpresaId($findUsuarioRequest->getEmpresaId());
        $this->usuarioQuery->withActivo($findUsuarioRequest->getActivo());
        $this->usuarioQuery->includeRol($findUsuarioRequest->isIncludeRol());
        $this->usuarioQuery->withRolId($findUsuarioRequest->getRolId());
        //Orden
        $this->usuarioQuery->sort($findUsuarioRequest->getSortBy(), $findUsuarioRequest->getSort());
        //Total registros
        $totalCount = $this->usuarioQuery->totalCount();
        //Paginacion
        $this->usuarioQuery->paginate(
            $findUsuarioRequest->getPaginate(),
            $findUsuarioRequest->getItemsToShow(),
            $findUsuarioRequest->getPage()
        );

        //Ejecutar query
        $usuarios = $this->usuarioQuery->execute();

        //Mapear query
        $usuariosResponse = $this->usuarioMapper->getMapper()->mapMultiple($usuarios, UsuarioResponse::class);

        //Find data response
        $findUsuariosDataResponse = new FindDataResponse();
        $findUsuariosDataResponse->setResults($usuariosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findUsuarioRequest->getPage());
        $paginationResponse->setPageCount(count($usuariosResponse));
        $paginationResponse->setPageSize($findUsuarioRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findUsuariosResponse = new  FindResponse();
        $findUsuariosResponse->setData($findUsuariosDataResponse);
        $findUsuariosResponse->setPagination($paginationResponse);

        return response()->json($findUsuariosResponse);

    }

    public function get($id)
    {
        /* @var $usuario Usuario*/
        $usuario = $this->usuarioRepository->get($id);
        ProxyExtensions::load($usuario->getRol());
        ProxyExtensions::toArray($usuario->getRol()->getPermisos());
        ProxyExtensions::load($usuario->getEmpresa());
        $usuarioResponse = $this->usuarioMapper->getMapper()->map($usuario, UsuarioResponse::class);
        return response()->json($usuarioResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $usuario Usuario*/
        $usuarioRequest = new UsuarioRequest($request->all());
        $password = bcrypt($request->password);
        $fechaNacimiento = new \DateTime($request->fecha_nacimiento);
        $usuario = $this->usuarioMapper->getMapper()->map($usuarioRequest, Usuario::class);
        $usuario->setActivo(true);
        $usuario->setPassword($password);
        $usuario->setFechaNacimiento($fechaNacimiento);
        $this->usuarioRepository->add($usuario);
        $createResponse = new CreateResponse();
        $createResponse->setId($usuario->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $usuario Usuario*/
            $usuarioRequest = new UsuarioRequest($request->all());
            $usuario = $this->usuarioRepository->get($usuarioRequest->getId());
            $this->usuarioMapper->getMapper()->mapToObject($usuarioRequest, $usuario);
            if(!is_null($usuarioRequest->password)){
                $password = bcrypt($usuarioRequest->password);
                $usuario->setPassword($password);
            }
            $this->usuarioRepository->update($usuario);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $usuario Usuario*/

        //Eliminado logico
        try{
            $usuario = $this->usuarioRepository->get($id);
            $usuario->setActivo(false);
            $this->usuarioRepository->update($usuario);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}
