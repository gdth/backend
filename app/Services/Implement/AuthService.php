<?php
namespace App\Services\Implement;


use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Domain\Usuario;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Interfaces\IAuthService;
use App\Services\Mapper\Interfaces\IUsuarioMapper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AuthService implements IAuthService
{
    protected $usuarioRepository;
    protected $usuarioMapper;
    public function __construct(IUsuarioRepository $usuarioRepository, IUsuarioMapper $usuarioMapper)
    {
        $this->usuarioRepository = $usuarioRepository;
        $this->usuarioMapper = $usuarioMapper;
    }


    public function login(Request $request){
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeek(100);
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeek(100);
        $token->save();
        $usuario = $this->getDatosUsuario($request->user()->id);
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'user' => $usuario,
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    private function getDatosUsuario($usuarioId){
        $usuario = null;
        /* @var $usuario Usuario*/
        $usuario = $this->usuarioRepository->get($usuarioId);
        ProxyExtensions::load($usuario->getEmpresa());
        ProxyExtensions::load($usuario->getRol());
        $usuarioResponse = $this->usuarioMapper->getMapper()->map($usuario, UsuarioResponse::class);
        //dd($usuarioResponse);
        return $usuarioResponse;
    }
}
