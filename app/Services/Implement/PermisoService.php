<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IPermisoQuery;
use App\DataAccess\Repositories\Interfaces\IPermisoRepository;
use App\Domain\Permiso;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Permisos\FindPermisosRequest;
use App\Services\DTO\Permisos\PermisoRequest;
use App\Services\DTO\Permisos\PermisoResponse;
use App\Services\Interfaces\IPermisoService;
use App\Services\Mapper\Interfaces\IPermisoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class PermisoService implements IPermisoService
{
    protected $unitOfWork;
    protected $permisoRepository;
    protected $permisoQuery;
    protected $permisoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IPermisoRepository $permisoRepository,
        IPermisoQuery $permisoQuery,
        IPermisoMapper $permisoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->permisoRepository    = $permisoRepository;
        $this->permisoQuery         = $permisoQuery;
        $this->permisoMapper        = $permisoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findPermisoRequest = new FindPermisosRequest($request->all());
        //Query
        $this->permisoQuery->init();
        $this->permisoQuery->withName($findPermisoRequest->getName());
        $this->permisoQuery->withDisplayName($findPermisoRequest->getDisplayName());

        //Orden
        $this->permisoQuery->sort($findPermisoRequest->getSortBy(), $findPermisoRequest->getSort());

        //Total registros
        $totalCount = $this->permisoQuery->totalCount();

        //Paginacion
        $this->permisoQuery->paginate(
            $findPermisoRequest->getPaginate(),
            $findPermisoRequest->getItemsToShow(),
            $findPermisoRequest->getPage()
        );

        //Ejecutar query
        $permisos = $this->permisoQuery->execute();

        //Mapear query
        $permisosResponse = $this->permisoMapper->getMapper()->mapMultiple($permisos, PermisoResponse::class);

        //Find data response
        $findPermisosDataResponse = new FindDataResponse();
        $findPermisosDataResponse->setResults($permisosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findPermisoRequest->getPage());
        $paginationResponse->setPageCount(count($permisosResponse));
        $paginationResponse->setPageSize($findPermisoRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findPermisosResponse = new  FindResponse();
        $findPermisosResponse->setData($findPermisosDataResponse);
        $findPermisosResponse->setPagination($paginationResponse);

        return response()->json($findPermisosResponse);

    }

    public function get($id)
    {
        /* @var $permiso Permiso*/
        $permiso = $this->permisoRepository->get($id);
        $permisoResponse = $this->permisoMapper->getMapper()->map($permiso, PermisoResponse::class);
        return response()->json($permisoResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $permiso Permiso*/
        $permisoRequest = new PermisoRequest($request->all());
        $permiso = $this->permisoMapper->getMapper()
            ->map($permisoRequest, Permiso::class);
        $this->permisoRepository->add($permiso);
        $createResponse = new CreateResponse();
        $createResponse->setId($permiso->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $permiso Permiso*/
        $success = false;
        try{
            $permisoRequest = new PermisoRequest($request->all());
            $permiso = $this->permisoRepository->get($permisoRequest->getId());
            $this->permisoMapper->getMapper()->mapToObject($permisoRequest, $permiso);
            $this->permisoRepository->update($permiso);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        /* @var $permiso Permiso*/
        $success = false;

        //Eliminado DB
        $permiso = $this->permisoRepository->get($id);
        try{

            if ($permiso !== null) {
                $this->permisoRepository->remove($permiso);
                $success = true;
            }
        }
        catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}
