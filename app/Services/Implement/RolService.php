<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IRolQuery;
use App\DataAccess\Repositories\Interfaces\IPermisoRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Permiso;
use App\Domain\Rol;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Permisos\PermisoResponse;
use App\Services\DTO\Roles\CreateRolResponse;
use App\Services\DTO\Roles\FindRolesRequest;
use App\Services\DTO\Roles\RolRequest;
use App\Services\DTO\Roles\RolResponse;
use App\Services\Interfaces\IRolService;
use App\Services\Mapper\Interfaces\IRolMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class RolService implements IRolService
{
    protected $unitOfWork;
    protected $rolRepository;
    protected $rolQuery;
    protected $rolMapper;
    protected $permisoRepository;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IRolRepository $rolRepository,
        IRolQuery $rolQuery,
        IRolMapper $rolMapper,
        IPermisoRepository $permisoRepository)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->rolRepository    = $rolRepository;
        $this->rolQuery         = $rolQuery;
        $this->rolMapper        = $rolMapper;
        $this->permisoRepository = $permisoRepository;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findRolRequest = new FindRolesRequest($request->all());
        //Query
        $this->rolQuery->init();
        $this->rolQuery->withName($findRolRequest->getName());
        $this->rolQuery->withDisplayName($findRolRequest->getDisplayName());
        $this->rolQuery->withIsDelete($findRolRequest->getIsDelete());
        //Orden
        $this->rolQuery->sort($findRolRequest->getSortBy(), $findRolRequest->getSort());
        //Total registros
        $totalCount = $this->rolQuery->totalCount();
        //Paginacion
        $this->rolQuery->paginate(
            $findRolRequest->getPaginate(),
            $findRolRequest->getItemsToShow(),
            $findRolRequest->getPage()
        );

        //Ejecutar query
        $roles = $this->rolQuery->execute();
        /*dd($roles);*/

        //Mapear query
        $rolesResponse = $this->rolMapper->getMapper()->mapMultiple($roles, RolResponse::class);

        //Find data response
        $findRolesDataResponse = new FindDataResponse();
        $findRolesDataResponse->setResults($rolesResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findRolRequest->getPage());
        $paginationResponse->setPageCount(count($rolesResponse));
        $paginationResponse->setPageSize($findRolRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findRolesResponse = new  FindResponse();
        $findRolesResponse->setData($findRolesDataResponse);
        $findRolesResponse->setPagination($paginationResponse);

        return response()->json($findRolesResponse);

    }

    public function get($id)
    {
        /* @var $rol Rol*/
        $rol = $this->rolRepository->get($id);
        ProxyExtensions::toArray($rol->getPermisos());
        $rolResponse = $this->rolMapper->getMapper()
            ->map($rol, RolResponse::class);
        return response()->json($rolResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        $rolRequest = $this->rolMapper->getMapper()->map($request, RolRequest::class);
        /* @var $rol Rol*/
        $rol = $this->rolMapper->getMapper()
            ->map($rolRequest, Rol::class);
        $permisosRequest = $rolRequest->getPermisos();
        $permisos = array();
        foreach ($permisosRequest as $permisoRequest){
            $permiso = $this->getPermiso($permisoRequest);
            $rol->addPermission($permiso);
            $permisos[] = array('id' => $permiso->getId());
        }
        $rol->setIsDelete(false);
        $this->rolRepository->add($rol);
        $createRolResponse = new CreateRolResponse();
        $createRolResponse->setId($rol->getId());
        $createRolResponse->setPermisos($permisos);
        return response()->json($createRolResponse);
    }

    /**
     * @param array $permisoRequest
     */
    private function getPermiso($permisoRequest){
        /** @var Permiso $permisoRequest*/
        $permiso = $this->permisoRepository->get($permisoRequest->getId());
        return $permiso;
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $rol Rol*/
            $rolRequest = $this->rolMapper->getMapper()->map($request, RolRequest::class);
            $rol = $this->rolRepository->get($rolRequest->getId());
            ProxyExtensions::toArray($rol->getPermisos());
            $this->rolMapper->getMapper()->mapToObject($rolRequest, $rol);
            //Elimino los permisos guardados previamente
            $rol->removePermissions($rol->getPermisos());
            //Obtengo los permisos nuevos seleccionados
            $permisosRequest = $rolRequest->getPermisos();
            //Obtengo los nuevos permisos
            $permisos = array();
            $errorPermisos = false;
            foreach ($permisosRequest as $permisoRequest){
                $permiso = $this->getPermiso($permisoRequest);
                if($permiso == null){
                    $errorPermisos = true;
                    break;
                }
                $rol->addPermission($permiso);
                $permisos[] = array('id' => $permiso->getId());
            }

            if(!$errorPermisos){
                $this->rolRepository->update($rol);
                $success = true;
            }
            $createRolResponse = new CreateRolResponse();
            $createRolResponse->setId($rol->getId());
            $createRolResponse->setPermisos($permisos);
            $createRolResponse->setSuccess($success);
            return response()->json($createRolResponse);
        }catch (Exception $exception){
            throw $exception;
        }
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;
        /* @var $rol Rol*/
        //Eliminado logico
        try{
            $rol = $this->rolRepository->get($id);
            $rol->setIsDelete(true);
            $this->rolRepository->update($rol);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}
