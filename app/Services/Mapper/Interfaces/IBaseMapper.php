<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/31/18
 * Time: 12:09 PM
 */

namespace App\Services\Mapper\Interfaces;


use AutoMapperPlus\AutoMapper;

interface IBaseMapper
{
    public function initialize();
    /**
     * @return AutoMapper
     */
    public function getMapper();
}