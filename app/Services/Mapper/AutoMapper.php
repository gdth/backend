<?php


namespace App\Services\Mapper;


use App\Services\Mapper\Operations\Implement\MapInstanceTo;
//use App\Services\Mapper\Operations\Implement\MapEntityInstancesTo;
use App\Services\Mapper\Operations\Implement\MapSourceTo;
use AutoMapperPlus\Configuration\MappingInterface;
use AutoMapperPlus\MappingOperation\Implementations\MapTo;

class AutoMapper extends \AutoMapperPlus\AutoMapper
{
    /**
     * Performs the actual transferring of properties.
     *
     * @param $source
     * @param $destination
     * @param MappingInterface $mapping
     * @return mixed
     *   The destination object with mapped properties.
     */
    protected function doMap($source, $destination, MappingInterface $mapping, array $context = [])
    {
        $propertyNames = $mapping->getTargetProperties($destination, $source);
        foreach ($propertyNames as $propertyName) {
            $mappingOperation = $mapping->getMappingOperationFor($propertyName);

            // @todo: find another solution to this hacky implementation of
            // recursive mapping.
            if ($mappingOperation instanceof MapTo || $mappingOperation instanceof MapInstanceTo || $mappingOperation instanceof MapSourceTo) {
                $mappingOperation->setMapper($this);
            }

            $mappingOperation->mapProperty(
                $propertyName,
                $source,
                $destination
            );
        }

        return $destination;
    }
}