<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Empresa;
use App\Domain\Permiso;
use App\Domain\Rol;
use App\Domain\Usuario;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Permisos\PermisoResponse;
use App\Services\DTO\Roles\RolResponse;
use App\Services\DTO\Usuarios\UsuarioRequest;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Mapper\Interfaces\IUsuarioMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class UsuarioMapper implements IUsuarioMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();
        /*USUARIO*/
        $config->registerMapping(Usuario::class, UsuarioResponse::class)
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('rol', Operation::mapInstanceTo(RolResponse::class))
            ->forMember('fecha_nacimiento',   Operation::mapAsDate());

        /*EMPRESA*/
        $config->registerMapping(Empresa::class, EmpresaResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        /*ROL*/
        $config->registerMapping(Rol::class, RolResponse::class)
            ->forMember('permisos', Operation::mapInstanceTo(PermisoResponse::class));

        /*PERMISO*/
        $config->registerMapping(Permiso::class, PermisoResponse::class);

        /*CREATE/UPDATE*/
        $config->registerMapping(UsuarioRequest::class, Usuario::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, 'empresa_id'))
            ->forMember('rol', Operation::mapAsEntity(Rol::class, 'rol_id'));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}