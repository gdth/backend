<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Permiso;
use App\Domain\Rol;
use App\Services\DTO\Permisos\PermisoRequest;
use App\Services\DTO\Permisos\PermisoResponse;
use App\Services\DTO\Roles\RolRequest;
use App\Services\DTO\Roles\RolResponse;
use App\Services\Mapper\Interfaces\IRolMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;
use Illuminate\Http\Request;


class RolMapper implements IRolMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();
        /*ROL*/
        $config->registerMapping(Rol::class, RolResponse::class)
            ->forMember('permisos', Operation::mapInstanceTo(PermisoResponse::class));


        /*ROL Y PERMISOS REQUEST*/
        $config
            ->registerMapping(Request::class, RolRequest::class)
            ->forMember('permisos',          function(Request $request){
                $array = [];
                if(isset($request->permisos)){
                    foreach ($request->permisos as $permisoR){
                        $permiso = $this->getMapper()
                            ->map((object)$permisoR, PermisoRequest::class);
                        array_push($array, $permiso);
                    }
                }

                return $array;
            });
        $config
            ->registerMapping( \stdClass::class, PermisoRequest::class);

        /*CREATE*/
        $config->registerMapping(RolRequest::class, Rol::class)
            ->forMember('permisos',     Operation::ignore());

        /*CREATE PERMISO*/
        /*$config->registerMapping(PermisoRequest::class, Permiso::class)
            ->forMember('id', Operation::ignore());*/

        /*PERMISOS*/
        $config->registerMapping(Permiso::class, PermisoResponse::class);
        /*$config->registerMapping(Permiso::class, Permiso::class);*/

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}