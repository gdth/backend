<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CCiudades\CCiudadRequest;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\Mapper\Interfaces\ICCiudadMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CCiudadMapper implements ICCiudadMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /*CIUDAD*/
        $config
            ->registerMapping(CCiudad::class, CCiudadResponse::class)
            ->forMember('estado', Operation::mapInstanceTo(CEstadoResponse::class));

        /*ESTADO*/
        $config
            ->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember("ciudades", Operation::ignore());

        /*CREATE/UPDATE*/
        $config
            ->registerMapping(CCiudadRequest::class, CCiudad::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("estado", Operation::mapAsEntity(CEstado::class, "estado_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);
        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}