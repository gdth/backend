<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CEstados\CEstadoRequest;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\Mapper\Interfaces\ICEstadoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CEstadoMapper implements ICEstadoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config
            ->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember('ciudades',      Operation::mapInstanceTo(CCiudadResponse::class));

        /*CIUDAD*/
        $config
            ->registerMapping(CCiudad::class, CCiudadResponse::class)
            ->forMember('estado', Operation::ignore());

        /*CREATE*/
        $config->registerMapping(CEstadoRequest::class, CEstado::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('ciudades',     Operation::ignore());

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}