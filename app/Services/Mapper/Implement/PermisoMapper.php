<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Permiso;
use App\Services\DTO\Permisos\PermisoRequest;
use App\Services\DTO\Permisos\PermisoResponse;
use App\Services\Mapper\Interfaces\IPermisoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class PermisoMapper implements IPermisoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /*PERMISO*/
        $config->registerMapping(Permiso::class, PermisoResponse::class);

        /*CREATE/UPDATE*/
        $config->registerMapping(PermisoRequest::class, Permiso::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt());

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}