<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\Empresa;
use App\Domain\CEstado;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\Empresas\EmpresaRequest;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\Mapper\Interfaces\IEmpresaMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class EmpresaMapper implements IEmpresaMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /*EMPRESA*/
        $config->registerMapping(Empresa::class, EmpresaResponse::class)
            ->forMember('estado', Operation::mapInstanceTo(CEstadoResponse::class))
            ->forMember('ciudad', Operation::mapInstanceTo(CCiudadResponse::class));

        /*ESTADO*/
        $config
            ->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember("ciudades", Operation::ignore());

        /*CIUDAD*/
        $config
            ->registerMapping(CCiudad::class, CCiudadResponse::class)
            ->forMember('estado', Operation::ignore());
        
        /*CREATE/UPDATE*/
        $config->registerMapping(EmpresaRequest::class, Empresa::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("estado", Operation::mapAsEntity(CEstado::class, "estado_id"))
            ->forMember("ciudad", Operation::mapAsEntity(CCiudad::class, "ciudad_id"));


        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}