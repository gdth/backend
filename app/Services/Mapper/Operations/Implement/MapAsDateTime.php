<?php
namespace App\Services\Mapper\Operations\Implement;
use App\Infrastructure\DateExtensions;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;

class MapAsDateTime extends DefaultMappingOperation
{
    protected $format;

    /**
     * MapAsDateTime constructor.
     * @param string $format
     */
    public function __construct(string $format = 'Y-m-d')
    {
        $this->format = $format;
    }


    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $sourceValue = $this->getSourceValue($source, $propertyName);
        $this->setDestinationValue($destination, $propertyName, DateExtensions::getDateFormatted($sourceValue, $this->format));
        //$this->setDestinationValue($destination, $propertyName, \DateTime::createFromFormat($this->format, $sourceValue));
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}