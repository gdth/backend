<?php
namespace App\Services\Mapper\Operations\Implement;
use App\Infrastructure\DateExtensions;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;

class MapStringToTime extends DefaultMappingOperation
{
    protected $format;

    /**
     * MapAsDateTime constructor.
     * @param string $format
     */
    public function __construct(string $format = 'H:i:s')
    {
        $this->format = $format;
    }


    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $sourceValue = $this->getSourceValue($source, $propertyName);
        $this->setDestinationValue($destination, $propertyName, DateExtensions::getStringToTimeFormatted($sourceValue));
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}