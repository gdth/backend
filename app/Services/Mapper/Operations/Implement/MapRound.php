<?php


namespace App\Services\Mapper\Operations\Implement;


use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;

class MapRound extends DefaultMappingOperation
{
    protected $precision;

    /**
     * MapRound constructor.
     * @param $precision
     */
    public function __construct(int $precision = 2)
    {
        $this->precision = $precision;
    }

    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $sourceValue = $this->getSourceValue($source, $propertyName);
        $this->setDestinationValue($destination, $propertyName,  round($sourceValue, $this->precision));
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}