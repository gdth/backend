<?php


namespace App\Services\Mapper\Operations\Implement;


use App\Services\DTO\DataTransferObject;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;
use Illuminate\Http\Request;

class MapIfRequestHasIt extends DefaultMappingOperation
{
    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $sourceValue = $this->getSourceValue($source, $propertyName);

        /* @var $source DataTransferObject*/
        if($source->has($propertyName)){
            $date = \DateTime::createFromFormat("Y-m-d", $sourceValue);
            $sourceValue = $date ? $date : $sourceValue;
            $this->setDestinationValue($destination, $propertyName, $sourceValue);
        }

    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}