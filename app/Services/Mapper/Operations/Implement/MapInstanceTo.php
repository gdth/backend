<?php
namespace App\Services\Mapper\Operations\Implement;

use App\Infrastructure\ProxyExtensions;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;
use function Functional\map;

class MapInstanceTo extends DefaultMappingOperation
{
    /**
     * @var string
     */
    protected $destinationClass;

    protected $initializeProxy = false;

    /**
     * @var AutoMapperInterface
     */
    protected $mapper;


    /**
     * MapInstanceTo constructor.
     * @param mixed $destinationClass
     * @param bool $initializeProxy
     */
    public function __construct($destinationClass, bool $initializeProxy)
    {
        $this->initializeProxy = $initializeProxy;
        $this->destinationClass = $destinationClass;
    }

    /**
     * @return string
     */
    public function getDestinationClass(): string
    {
        return $this->destinationClass;
    }

    /**
     * @param AutoMapperInterface $mapper
     */
    public function setMapper(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    protected function getSourceValue($source, string $propertyName)
    {
        $sourceValue = $this->getPropertyAccessor()->getProperty($source, $propertyName);


        if($this->initializeProxy===true){
            ProxyExtensions::initialize($sourceValue);
        }

        $shouldBeMapped = $this->shouldBeMapped($sourceValue);

        if(is_string($this->destinationClass)){
            if($shouldBeMapped){
                return $this->isCollection($sourceValue)
                    ? $this->mapper->mapMultiple($sourceValue, $this->destinationClass)
                    : $this->mapper->map($sourceValue, $this->destinationClass);
            }
        }
        else if(is_array($this->destinationClass)){
            if($shouldBeMapped){
                return $this->isCollection($sourceValue)
                    ? $this->mapMultipleWithMultipleDestinationClasses($sourceValue, $this->destinationClass)
                    : $this->mapWithMultipleDestinationClass($sourceValue, $this->destinationClass);

            }
        }
    }

    protected function shouldBeMapped($sourceValue){
        return ProxyExtensions::isInitialized($sourceValue) || ProxyExtensions::isEntity($sourceValue);
    }

    protected function mapMultipleWithMultipleDestinationClasses($sourceCollection, $destinationClasses){
        return map($sourceCollection, function ($source) use ($destinationClasses) {
            return  $this->mapWithMultipleDestinationClass($source, $destinationClasses);
        });

    }

    protected function mapWithMultipleDestinationClass($instance, $destinationClasses){
        $map = null;
        foreach ($destinationClasses as $entity => $destinationClass){
            if($instance instanceof $entity){
               $map = $this->mapper->map($instance, $destinationClass);
            }
        }
        return $map;
    }

    /**
     * Checks if the provided input is a collection.
     * @todo: might want to move this outside of this class.
     *
     * @param $variable
     * @return bool
     */
    protected function isCollection($variable): bool
    {
        return is_array($variable) || $variable instanceof \Traversable;
    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}