<?php
namespace App\Services\Mapper\Operations\Implement;

use App\Infrastructure\ProxyExtensions;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;
use function Functional\map;

class MapSourceTo extends DefaultMappingOperation
{
    /**
     * @var string
     */
    protected $destinationClass;


    /**
     * @var AutoMapperInterface
     */
    protected $mapper;


    /**
     * MapInstanceTo constructor.
     * @param mixed $destinationClass
     */
    public function __construct($destinationClass)
    {
        $this->destinationClass = $destinationClass;
    }

    /**
     * @return string
     */
    public function getDestinationClass(): string
    {
        return $this->destinationClass;
    }

    /**
     * @param AutoMapperInterface $mapper
     */
    public function setMapper(AutoMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritdoc
     */
    protected function getSourceValue($source, string $propertyName)
    {
        if(is_string($this->destinationClass)){
                return $this->isCollection($source)
                    ? $this->mapper->mapMultiple($source, $this->destinationClass)
                    : $this->mapper->map($source, $this->destinationClass);
        }
        else if(is_array($this->destinationClass)){
                return $this->isCollection($source)
                    ? $this->mapMultipleWithMultipleDestinationClasses($source, $this->destinationClass)
                    : $this->mapWithMultipleDestinationClass($source, $this->destinationClass);

        }
    }

    protected function mapMultipleWithMultipleDestinationClasses($sourceCollection, $destinationClasses){
        return map($sourceCollection, function ($source) use ($destinationClasses) {
            return  $this->mapWithMultipleDestinationClass($source, $destinationClasses);
        });

    }

    protected function mapWithMultipleDestinationClass($instance, $destinationClasses){
        $map = null;
        foreach ($destinationClasses as $entity => $destinationClass){
            if($instance instanceof $entity){
               $map = $this->mapper->map($instance, $destinationClass);
            }
        }
        return $map;
    }

    /**
     * Checks if the provided input is a collection.
     * @todo: might want to move this outside of this class.
     *
     * @param $variable
     * @return bool
     */
    protected function isCollection($variable): bool
    {
        return is_array($variable) || $variable instanceof \Traversable;
    }

}