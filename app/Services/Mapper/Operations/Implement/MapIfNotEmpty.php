<?php


namespace App\Services\Mapper\Operations\Implement;


use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;

class MapIfNotEmpty  extends DefaultMappingOperation
{

    /**
     * MapAsDate constructor.
     * @param string $format
     */
    public function __construct()
    {

    }


    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $sourceValue = $this->getSourceValue($source, $propertyName);

        if(!empty($sourceValue)){
            $this->setDestinationValue($destination, $propertyName, $sourceValue);
        }

    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}