<?php


namespace App\Services\Mapper\Operations\Implement;


use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Configs\UnitOfWork;
use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\DefaultMappingOperation;
use function resolve;

class MapAsEntity extends DefaultMappingOperation
{
    protected $entityClass;
    protected $sourcePropertyName;
    /**
     * MapAsEntity constructor.
     */
    public function __construct($entityClass, $sourcePropertyName)
    {
        $this->entityClass = $entityClass;
        $this->sourcePropertyName = $sourcePropertyName;
    }


    /**
     * @param string $propertyName
     * @param $source
     * @param $destination
     * @return void
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        if($source->has($this->sourcePropertyName)){
            /* @var $unitOfWork UnitOfWork*/

            $unitOfWork = resolve(IUnitOfWork::class);

            $sourceValue = $this->getSourceValue($source, $this->sourcePropertyName ?? $propertyName);

            $repository = $unitOfWork->getEntityManager()->getRepository($this->entityClass);

            if(!empty($sourceValue)){
                $result = $repository->find($sourceValue);
                $this->setDestinationValue($destination, $propertyName, $result);
            }

        }


    }

    /**
     * @param Options $options
     */
    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }
}