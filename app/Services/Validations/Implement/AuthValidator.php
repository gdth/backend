<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 11:46 AM
 */

namespace App\Services\Validations\Implement;


use App\Services\Validations\Interfaces\IAuthValidator;
use Illuminate\Http\Request;

class AuthValidator implements IAuthValidator
{
    use ValidatorBase;

    public function validateLogin(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ];
        $messages = [
            'email' => "El email es requerido",
            'password' => "El password es requerido",
        ];

        $this->validate($attributes, $rules, $messages);
    }
}