<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 03:27 PM
 */

namespace App\Services\DTO\Roles;
use App\Services\DTO\DataTransferObject;

class RolResponse extends DataTransferObject implements IRolResponse
{
    protected $id = 0;
    protected $name = '';
    protected $display_name = '';
    protected $description = '';
    protected $is_delete = false;
    protected $permisos = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    /**
     * @param string $display_name
     */
    public function setDisplayName(?string $display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function getIsDelete(): ?bool
    {
        return $this->is_delete;
    }

    /**
     * @param bool $is_delete
     */
    public function setIsDelete(?bool $is_delete)
    {
        $this->is_delete = $is_delete;
    }

    /**
     * @return array|null $permisos
     */
    public function getPermisos(): ?array
    {
        return $this->permisos;
    }

    /**
     * @param array|null $permisos
     */
    public function setPermisos(?array $permisos)
    {
        $this->permisos = $permisos;
    }
}