<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 21/12/18
 * Time: 10:00 AM
 */

namespace App\Services\DTO\Roles;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface ICreateRolResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);
    public function getPermisos(): ?array;
    public function setPermisos(?array $permisos);
}