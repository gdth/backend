<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 03:39 PM
 */

namespace App\Services\DTO\Roles;
use App\Services\DTO\DataTransferObject;


class RolRequest extends DataTransferObject implements IRolRequest
{
    protected $id = 0;
    protected $name = '';
    protected $display_name = '';
    protected $description = '';
    protected $is_delete = false;
    protected $permisos = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return|null string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @return|null string
     */
    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    /**
     * @param string|null $display_name
     */
    public function setDisplayName(?string $display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return|null string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function getIsDelete(): ?bool
    {
        return $this->is_delete;
    }

    /**
     * @param bool $is_delete
     */
    public function setIsDelete(?bool $is_delete)
    {
        $this->is_delete = $is_delete;
    }

    /**
     * @return array|null $permisos
     */
    public function getPermisos(): ?array
    {
        return $this->permisos;
    }

    /**
     * @param array|null $permisos
     */
    public function setPermisos(?array $permisos)
    {
        $this->permisos = $permisos;
    }
}