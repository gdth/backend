<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 03:41 PM
 */

namespace App\Services\DTO\Roles;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;
use Aedart\DTO\DataTransferObject;

class FindRolesRequest extends DataTransferObject implements IFindRolesRequest
{
    use TFindBaseRequest;

    protected $name = '';
    protected $display_name = '';
    protected $is_delete = false;

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    /**
     * @param string $display_name
     */
    public function setDisplayName(?string $display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return string
     */
    public function getIsDelete():? string
    {
        return $this->is_delete;
    }

    /**
     * @param string $is_delete
     */
    public function setIsDelete(?string $is_delete): void
    {
        $this->is_delete = $is_delete;
    }
}