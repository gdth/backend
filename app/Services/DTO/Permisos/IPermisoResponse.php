<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 11:23 AM
 */

namespace App\Services\DTO\Permisos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IPermisoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getName(): ?string;
    public function setName(?string $name);

    public function getDisplayName(): ?string;
    public function setDisplayName(?string $display_name);

    public function getDescription(): ?string;
    public function setDescription(?string $description);
}