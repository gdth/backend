<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 11:24 AM
 */

namespace App\Services\DTO\Permisos;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindPermisosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getName(): ?string;
    public function setName(?string $name);

    public function getDisplayName(): ?string;
    public function setDisplayName(?string $display_name);
}