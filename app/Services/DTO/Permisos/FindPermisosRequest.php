<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Permisos;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindPermisosRequest extends DataTransferObject implements IFindPermisosRequest
{
    use TFindBaseRequest;

    protected $name = '';
    protected $display_name = '';

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    /**
     * @param string $display_name
     */
    public function setDisplayName(?string $display_name)
    {
        $this->display_name = $display_name;
    }
}