<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 01:14 PM
 */

namespace App\Services\DTO\Usuarios;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindUsuariosRequest extends DataTransferObject implements IFindUsuariosRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $nombre_completo = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $titulo = '';
    protected $puesto = '';
    protected $fecha_nacimiento = '';
    protected $min_fecha_nacimiento = '';
    protected $max_fecha_nacimiento = '';
    protected $telefono = '';
    protected $telefono_oficina = '';
    protected $extension = '';
    protected $celular = '';
    protected $email = '';
    protected $empresa_id = null;
    protected $include_empresa = false;
    protected $activo = '';
    protected $rol_id = null;
    protected $include_rol = false;
    protected $rol_nombre = [];

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getNombreCompleto(): ?string
    {
        return $this->nombre_completo;
    }

    /**
     * @param string $nombre_completo
     */
    public function setNombreCompleto(?string $nombre_completo): void
    {
        $this->nombre_completo= $nombre_completo;
    }

    /**
     * @return string
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno): void
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno): void
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return string
     */
    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo(?string $titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    /**
     * @param string $puesto
     */
    public function setPuesto(?string $puesto): void
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento): void
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string
     */
    public function getMinFechaNacimiento(): ?string
    {
        return $this->min_fecha_nacimiento;
    }

    /**
     * @param string $min_fecha_nacimiento
     */
    public function setMinFechaNacimiento(?string $min_fecha_nacimiento): void
    {
        $this->min_fecha_nacimiento = $min_fecha_nacimiento;
    }

    /**
     * @return string
     */
    public function getMaxFechaNacimiento(): ?string
    {
        return $this->max_fecha_nacimiento;
    }

    /**
     * @param string $max_fecha_nacimiento
     */
    public function setMaxFechaNacimiento(?string $max_fecha_nacimiento): void
    {
        $this->max_fecha_nacimiento = $max_fecha_nacimiento;
    }

    /**
     * @return string
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono(?string $telefono): void
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getTelefonoOficina(): ?string
    {
        return $this->telefono_oficina;
    }

    /**
     * @param string $telefono_oficina
     */
    public function setTelefonoOficina(?string $telefono_oficina): void
    {
        $this->telefono_oficina = $telefono_oficina;
    }

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(?string $extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getCelular(): ?string
    {
        return $this->celular;
    }

    /**
     * @param string $celular
     */
    public function setCelular(?string $celular): void
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?string
    {
        return $this->activo;
    }
    /**
     * @param string $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int
     */
    public function getRolId(): ?int
    {
        return $this->rol_id;
    }

    /**
     * @param string $rol_id
     */
    public function setRolId(?string $rol_id): void
    {
        $this->rol_id = $rol_id;
    }

    /**
     * @return bool
     */
    public function isIncludeRol(): ?bool
    {
        return $this->include_rol;
    }

    /**
     * @param string|null $include_rol
     */
    public function setIncludeRol(?string $include_rol): void
    {
        $this->include_rol = $include_rol;
    }

    /**
     * @return array|null
     */
    public function getRolNombre(): ?array
    {
        return $this->rol_nombre;
    }

    /**
     * @param array|null $rol_nombre
     */
    public function setRolNombre(?array $rol_nombre): void
    {
        $this->rol_nombre = $rol_nombre;
    }
}