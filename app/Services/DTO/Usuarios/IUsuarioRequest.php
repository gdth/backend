<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 10:52 AM
 */

namespace App\Services\DTO\Usuarios;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IUsuarioRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getApellidoPaterno(): ?string;
    public function setApellidoPaterno(?string $apellido_paterno);

    public function getApellidoMaterno(): ?string;
    public function setApellidoMaterno(?string $apellido_materno);

    public function getTitulo(): ?string;
    public function setTitulo(?string $titulo);

    public function getPuesto(): ?string;
    public function setPuesto(?string $puesto);

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono);

    public function getTelefonoOficina(): ?string;
    public function setTelefonoOficina(?string $telefono_oficina);

    public function getExtension(): ?string;
    public function setExtension(?string $extension);

    public function getCelular(): ?string;
    public function setCelular(?string $celular);

    public function getEmail(): ?string;
    public function setEmail(?string $email): void;

    public function getFechaNacimiento(): ?string;
    public function setFechaNacimiento(?string $fecha_nacimiento);

    public function getPassword(): ?string;
    public function setPassword(?string $password);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getActivo(): ?bool;
    public function setActivo(?bool $activo);

    public function getPrimerInicioSesion(): ?bool;
    public function setPrimerInicioSesion(?bool $primer_inicio_sesion);

    public function getRolId(): ?int;
    public function setRolId(?int $rol_id);
}