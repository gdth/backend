<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 12:43 PM
 */

namespace App\Services\DTO\Usuarios;
use App\Services\DTO\DataTransferObject;

class UsuarioRequest extends DataTransferObject implements IUsuarioRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $titulo = '';
    protected $puesto = '';
    protected $fecha_nacimiento = '';
    protected $telefono = '';
    protected $telefono_oficina = '';
    protected $extension = '';
    protected $password = null;
    protected $celular = '';
    protected $email ='';
    protected $empresa_id = 0;
    protected $activo = true;
    protected $primer_inicio_sesion = true;
    protected $rol_id = 0;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string|null $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno)
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string|null
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string|null $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno)
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return string|null
     */
    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    /**
     * @param string|null $titulo
     */
    public function setTitulo(?string $titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string|null
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    /**
     * @param string|null $puesto
     */
    public function setPuesto(?string $puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string|null
     */
    public function getTelefonoOficina(): ?string
    {
        return $this->telefono_oficina;
    }

    /**
     * @param string|null $telefono_oficina
     */
    public function setTelefonoOficina(?string $telefono_oficina)
    {
        $this->telefono_oficina = $telefono_oficina;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string|null $extension
     */
    public function setExtension(?string $extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string|null
     */
    public function getCelular(): ?string
    {
        return $this->celular;
    }

    /**
     * @param string|null $celular
     */
    public function setCelular(?string $celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string|null $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password)
    {
        $this->password = $password;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return bool|null
     */
    public function getPrimerInicioSesion(): ?bool
    {
        return $this->primer_inicio_sesion;
    }

    /**
     * @param bool|null $primer_inicio_sesion
     */
    public function setPrimerInicioSesion(?bool $primer_inicio_sesion)
    {
        $this->primer_inicio_sesion = $primer_inicio_sesion;
    }

    /**
     * @return int|null
     */
    public function getRolId(): ?int
    {
        return $this->rol_id;
    }

    /**
     * @param string|null $rol_id
     */
    public function setRolId(?int $rol_id)
    {
        $this->rol_id = $rol_id;
    }
}