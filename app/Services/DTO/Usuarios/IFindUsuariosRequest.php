<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 12:06 PM
 */

namespace App\Services\DTO\Usuarios;

use App\Services\DTO\Base\IFindBaseRequest;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IFindUsuariosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre): void;

    public function getNombreCompleto(): ?string;
    public function setNombreCompleto(?string $nombre_completo): void;

    public function getApellidoPaterno(): ?string;
    public function setApellidoPaterno(?string $apellido_paterno): void;

    public function getApellidoMaterno(): ?string;
    public function setApellidoMaterno(?string $apellido_materno): void;

    public function getTitulo(): ?string;
    public function setTitulo(?string $titulo): void;

    public function getPuesto(): ?string;
    public function setPuesto(?string $puesto): void;

    public function getFechaNacimiento(): ?string;
    public function setFechaNacimiento(?string $fecha_nacimiento): void;

    public function getMinFechaNacimiento(): ?string;
    public function setMinFechaNacimiento(?string $min_fecha_nacimiento): void;

    public function getMaxFechaNacimiento(): ?string;
    public function setMaxFechaNacimiento(?string $max_fecha_nacimiento): void;

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono): void;

    public function getTelefonoOficina(): ?string;
    public function setTelefonoOficina(?string $telefono_oficina): void;

    public function getExtension(): ?string ;
    public function setExtension(?string $extension): void;

    public function getCelular(): ?string;
    public function setCelular(?string $celular): void;

    public function getEmail(): ?string;
    public function setEmail(?string $email): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function getRolId(): ?int;
    public function setRolId(?string $rol_id): void;
    public function getRolNombre(): ?array;
    public function setRolNombre(?array $rol_nombre): void;

    public function isIncludeRol(): ?bool;
    public function setIncludeRol(?string $include_rol): void;
}