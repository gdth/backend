<?php
namespace App\Services\DTO;

use function array_has;

class DataTransferObject extends \Aedart\DTO\DataTransferObject
{
    protected $data = [];

    /**
     * {@inheritdoc}
     */
    public function populate(array $data = []) : void
    {
        $this->data = $data;
        parent::populate($data);
    }

    public function has($property){
        return array_has($this->data, $property);
    }
}