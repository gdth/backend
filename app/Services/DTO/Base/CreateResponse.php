<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/11/18
 * Time: 12:42 AM
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\DataTransferObject;

class CreateResponse extends DataTransferObject implements ICreateResponse
{
    protected $id = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id)
    {
        $this->id = $id;
    }
}