<?php

namespace App\Services\DTO\Base\Exceptions;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IExceptionResponse extends DataTransferObjectInterface
{
    public function getStatusCode(): ?int;
    public function setStatusCode(?int $statusCode): void;
    public function getMessage(): ?string;
    public function setMessage(?string $message): void;
    public function getData();
    public function setData($data): void;
    public function getType(): ?string;
    public function setType(?string $type): void;
    public function getDeveloperInfo(): ?IDeveloperExceptionInfoResponse;
    public function setDeveloperInfo(?IDeveloperExceptionInfoResponse $developer_info): void;

}