<?php


namespace App\Services\DTO\Base\Exceptions;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IDeveloperExceptionInfoResponse extends DataTransferObjectInterface
{
    public function getExceptionClass(): ?string;
    public function setExceptionClass(?string $exception_class): void;
    public function getTrace();
    public function setTrace($trace): void;
}