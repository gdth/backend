<?php


namespace App\Services\DTO\Base\Exceptions;


use Aedart\DTO\DataTransferObject;

class TraceExceptionResponse extends DataTransferObject implements ITraceExceptionResponse
{
    protected $file       = "";
    protected $line       = 0;
    protected $function   = "";
    protected $class      = "";
    protected $type       = "";


    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(?string $file): void
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getLine(): ?int
    {
        return $this->line;
    }

    /**
     * @param int $line
     */
    public function setLine(?int $line): void
    {
        $this->line = $line;
    }

    /**
     * @return string
     */
    public function getFunction(): ?string
    {
        return $this->function;
    }

    /**
     * @param string $function
     */
    public function setFunction(?string $function): void
    {
        $this->function = $function;
    }

    /**
     * @return string
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(?string $class): void
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }




}