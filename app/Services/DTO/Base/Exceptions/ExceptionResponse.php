<?php

namespace App\Services\DTO\Base\Exceptions;

use Aedart\DTO\DataTransferObject;

class ExceptionResponse extends DataTransferObject implements IExceptionResponse
{
    protected $status_code = 0;
    protected $message    = "";
    protected $data = null;
    protected $type = "";
    protected $developer_info = null;

    /**
     * @return int
     */
    public function getStatusCode(): ?int
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode(?int $status_code): void
    {
        $this->status_code = $status_code;
    }

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param null $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return IDeveloperExceptionInfoResponse
     */
    public function getDeveloperInfo(): ?IDeveloperExceptionInfoResponse
    {
        return $this->developer_info;
    }

    /**
     * @param IDeveloperExceptionInfoResponse $developer_info
     */
    public function setDeveloperInfo(?IDeveloperExceptionInfoResponse $developer_info): void
    {
        $this->developer_info = $developer_info;
    }




}