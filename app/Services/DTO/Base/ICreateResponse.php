<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/11/18
 * Time: 5:45 PM
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface ICreateResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);
}