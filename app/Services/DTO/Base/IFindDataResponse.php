<?php

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IFindDataResponse extends DataTransferObjectInterface
{
    public function getResults(): ?array;
    public function setResults(?array $results): void;
}