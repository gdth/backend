<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 18/05/18
 * Time: 18:51
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\DataTransferObject;

class CreateArrayResponse extends DataTransferObject implements ICreateArrayResponse
{
    protected $id          = 0;
    protected $id_frontend = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdFrontend(): ?int
    {
        return $this->id_frontend;
    }

    /**
     * @param int|null $id_frontend
     */
    public function setIdFrontend(?int $id_frontend)
    {
        $this->id_frontend = $id_frontend;
    }
}