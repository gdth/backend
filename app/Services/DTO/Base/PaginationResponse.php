<?php


namespace App\Services\DTO\Base;


use Aedart\DTO\DataTransferObject;

class PaginationResponse extends DataTransferObject implements IPaginationResponse
{
    protected $page = 0;
    protected $page_size = 0;
    protected $page_count = 0;
    protected $total_count = 0;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return PaginationResponse
     */
    public function setPage(int $page): PaginationResponse
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->page_size;
    }

    /**
     * @param int $page_size
     * @return PaginationResponse
     */
    public function setPageSize(int $page_size): PaginationResponse
    {
        $this->page_size = $page_size;
        return $this;
    }

    /**
     * @return int
     */
    public function getPageCount(): int
    {
        return $this->page_count;
    }

    /**
     * @param int $page_count
     * @return PaginationResponse
     */
    public function setPageCount(int $page_count): PaginationResponse
    {
        $this->page_count = $page_count;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int
    {
        return $this->total_count;
    }

    /**
     * @param int $total_count
     * @return PaginationResponse
     */
    public function setTotalCount(int $total_count): PaginationResponse
    {
        $this->total_count = $total_count;
        return $this;
    }


}