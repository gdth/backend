<?php
declare(strict_types=1);

namespace App\Services\DTO\Base;

use Aedart\DTO\DataTransferObject;

class SuccessResponse extends DataTransferObject implements ISuccessResponse
{

    /**
     * @var bool
     */
    protected $isSuccess = false;
    protected $data = null;

    /**
     * @param bool|null $isSuccess
     */
    public function setIsSuccess(?bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return bool|null
     */
    public function getIsSuccess(): ?bool
    {
        return $this->isSuccess;
    }

    /**
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }


}