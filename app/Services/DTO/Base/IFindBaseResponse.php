<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/11/18
 * Time: 12:47 AM
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IFindBaseResponse extends DataTransferObjectInterface
{
//    public function getResults(): ?array;
//    public function setResults(?array $results);
//    public function getTotalRecords(): ?int;
//    public function setTotalRecords(?int $total_records);
}