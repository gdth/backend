<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 10:21 AM
 */

namespace App\Services\DTO\CEstados;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;
use App\Services\DTO\CatalogosBase\FindCatalogosBaseRequest;


class FindCEstadosRequest extends FindCatalogosBaseRequest implements IFindCEstadosRequest
{
    use TFindBaseRequest;

    protected $include_ciudades = false;

    /**
     * @return bool
     */
    public function isIncludeCiudades(): ?bool
    {
        return StringExtensions::toBoolean($this->include_ciudades);
    }

    /**
     * @param bool $include_ciudades
     */
    public function setIncludeCiudades(?string $include_ciudades): void
    {
        $this->include_ciudades = $include_ciudades;
    }
}