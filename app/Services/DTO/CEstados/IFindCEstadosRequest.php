<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 11/12/18
 * Time: 01:20 PM
 */

namespace App\Services\DTO\CEstados;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;
use App\Services\DTO\CatalogosBase\IFindCatalogosBaseRequest;


interface IFindCEstadosRequest extends DataTransferObjectInterface, IFindBaseRequest, IFindCatalogosBaseRequest
{
    public function isIncludeCiudades(): ?bool;
    public function setIncludeCiudades(?string $include_ciudades):void;
}