<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 09:54 AM
 */

namespace App\Services\DTO\CEstados;
use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;


class CEstadoResponse extends CatalogoBaseResponse implements ICEstadoResponse
{
    protected $ciudades = null;

    /**
     * @return array|null
     */
    public function getCiudades(): ?array
    {
        return $this->ciudades;
    }

    /**
     * @param array|null $ciudades
     */
    public function setCiudades(?array $ciudades): void
    {
        $this->ciudades = $ciudades;
    }
}