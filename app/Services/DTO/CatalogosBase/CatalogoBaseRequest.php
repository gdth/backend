<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:11 PM
 */

namespace App\Services\DTO\CatalogosBase;
use App\Services\DTO\DataTransferObject;


class CatalogoBaseRequest extends DataTransferObject implements ICatalogoBaseRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $valor = '';
    protected $activo = true;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string|null $valor
     */
    public function setValor(?string $valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

}