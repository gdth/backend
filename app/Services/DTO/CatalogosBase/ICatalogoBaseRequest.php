<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:02 PM
 */

namespace App\Services\DTO\CatalogosBase;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface ICatalogoBaseRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);
    public function getValor(): ?string;
    public function setValor(?string $valor);
    public function getActivo():? bool;
    public function setActivo(?bool $activo);
}