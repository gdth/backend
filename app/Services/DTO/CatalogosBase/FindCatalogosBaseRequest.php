<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:13 PM
 */

namespace App\Services\DTO\CatalogosBase;
use Aedart\DTO\DataTransferObject;
use App\Services\DTO\Base\TFindBaseRequest;


class FindCatalogosBaseRequest extends DataTransferObject implements IFindCatalogosBaseRequest
{

    use TFindBaseRequest;

    protected $nombre = '';
    protected $valor = '';
    protected $activo = '';

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(?string $valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

}