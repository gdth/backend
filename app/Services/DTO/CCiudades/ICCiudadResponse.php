<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 11/12/18
 * Time: 01:10 PM
 */

namespace App\Services\DTO\CCiudades;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\CatalogosBase\ICatalogoBaseResponse;

interface ICCiudadResponse extends DataTransferObjectInterface, ICatalogoBaseResponse
{
    public function getEstado(): ?ICEstadoResponse;
    public function setEstado(?ICEstadoResponse $estado);
}