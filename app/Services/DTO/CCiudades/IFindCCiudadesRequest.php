<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 11/12/18
 * Time: 01:20 PM
 */

namespace App\Services\DTO\CCiudades;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;
use App\Services\DTO\CatalogosBase\IFindCatalogosBaseRequest;


interface IFindCCiudadesRequest extends DataTransferObjectInterface, IFindBaseRequest, IFindCatalogosBaseRequest
{
    public function getEstadoId(): ?int;
    public function setEstadoId(?string $estado_id):void;
    public function isIncludeEstado(): ?bool;
    public function setIncludeEstado(?string $include_estado): void;
}