<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 10:21 AM
 */

namespace App\Services\DTO\CCiudades;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;
use App\Services\DTO\CatalogosBase\FindCatalogosBaseRequest;


class FindCCiudadesRequest extends FindCatalogosBaseRequest implements IFindCCiudadesRequest
{
    use TFindBaseRequest;
    protected $estado_id = 0;
    protected $include_estado = false;

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return StringExtensions::toInt($this->estado_id);
    }

    /**
     * @param null|string $estado_id
     */
    public function setEstadoId(?string $estado_id): void
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estado);
    }

    /**
     * @param null|string $include_estado
     */
    public function setIncludeEstado(?string $include_estado): void
    {
        $this->include_estado = $include_estado;
    }
}