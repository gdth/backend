<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 10:06 AM
 */

namespace App\Services\DTO\CCiudades;
use App\Services\DTO\CatalogosBase\CatalogoBaseRequest;


class CCiudadRequest extends CatalogoBaseRequest implements ICCiudadRequest
{
    protected $estado_id = 0;

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setEstadoId(?int $estado_id)
    {
        $this->estado_id = $estado_id;
    }
}