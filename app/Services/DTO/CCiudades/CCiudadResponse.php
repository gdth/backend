<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 11:53 AM
 */

namespace App\Services\DTO\CCiudades;

use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;


class CCiudadResponse extends CatalogoBaseResponse implements ICCiudadResponse
{
    protected $estado = null;
    /**
     * @return ICEstadoResponse|null
     */
    public function getEstado(): ?ICEstadoResponse
    {
        return $this->estado;
    }

    /**
     * @param ICEstadoResponse|null $estado
     */
    public function setEstado(?ICEstadoResponse $estado)
    {
        $this->estado = $estado;
    }
}