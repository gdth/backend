<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 11/12/18
 * Time: 01:04 PM
 */

namespace App\Services\DTO\CCiudades;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseRequest;

interface ICCiudadRequest extends DataTransferObjectInterface, ICatalogoBaseRequest
{
    public function getEstadoId(): ?int;
    public function setEstadoId(?int $ciudad_id);
}