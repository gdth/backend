<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\Empresas;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\CEstados\ICEstadoResponse;


class EmpresaResponse extends DataTransferObject implements IEmpresaResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $telefono = '';
    protected $correo = '';
    protected $direccion = '';
    protected $estado = null;
    protected $ciudad = null;
    protected $activo = true;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->telefono = $correo;
    }

    /**
     * @return string
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return ICEstadoResponse|null
     */
    public function getEstado(): ?ICEstadoResponse
    {
        return $this->estado;
    }

    /**
     * @param ICEstadoResponse|null $estado
     */
    public function setEstado(?ICEstadoResponse $estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return ICCiudadResponse|null
     */
    public function getCiudad(): ?ICCiudadResponse
    {
        return $this->ciudad;
    }

    /**
     * @param ICCiudadResponse|null $ciudad
     */
    public function setCiudad(?ICCiudadResponse $ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}