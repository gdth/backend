<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\Empresas;
use App\Services\DTO\DataTransferObject;


class EmpresaRequest extends DataTransferObject implements IEmpresaRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $telefono = '';
    protected $correo = '';
    protected $direccion = '';
    protected $estado_id = 0;
    protected $ciudad_id = 0;
    protected $activo = true;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setEstadoId(?int $estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return $this->ciudad_id;
    }

    /**
     * @param int|null $ciudad_id
     */
    public function setCiudadId(?int $ciudad_id)
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}