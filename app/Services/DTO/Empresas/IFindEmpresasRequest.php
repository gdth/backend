<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\Empresas;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindEmpresasRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    /*public function getTelefono(): ?string;
    public function setTelefono(?string $telefono);*/

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    /*public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);*/

    public function getActivo():? string;
    public function setActivo(?string $activo): void;

    public function getEstadoId(): ?int;
    public function setEstadoId(?string $estado_id): void;
    public function isIncludeEstado(): ?bool;
    public function setIncludeEstado(?string $include_estado): void;

    public function getCiudadId(): ?int;
    public function setCiudadId(?string $ciudad_id): void;
    public function isIncludeCiudad(): ?bool;
    public function setIncludeCiudad(?string $include_ciudad): void;
}