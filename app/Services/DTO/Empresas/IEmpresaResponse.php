<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\Empresas;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\CEstados\ICEstadoResponse;

interface IEmpresaResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getEstado(): ?ICEstadoResponse;
    public function setEstado(?ICEstadoResponse $estado);

    public function getCiudad(): ?ICCiudadResponse;
    public function setCiudad(?ICCiudadResponse $ciudad);
}