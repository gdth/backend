<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Empresas;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindEmpresasRequest extends DataTransferObject implements IFindEmpresasRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
//    protected $telefono = '';
    protected $correo = '';
    /*protected $direccion = '';*/
    protected $estado_id = 0;
    protected $include_estado = false;
    protected $ciudad_id = 0;
    protected $include_ciudad = false;
    protected $activo = '';


    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    /*public function getTelefono(): ?string
    {
        return $this->telefono;
    }*/

    /**
     * @param string $telefono
     */
    /*public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }*/

    /**
     * @return string
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string
     */
    /*public function getDireccion(): ?string
    {
        return $this->direccion;
    }*/

    /**
     * @param string $direccion
     */
    /*public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }*/

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return StringExtensions::toInt($this->estado_id);
    }

    /**
     * @param null|string $estado_id
     */
    public function setEstadoId(?string $estado_id): void
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estado);
    }

    /**
     * @param null|string $include_estado
     */
    public function setIncludeEstado(?string $include_estado): void
    {
        $this->include_estado = $include_estado;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return StringExtensions::toInt($this->ciudad_id);
    }

    /**
     * @param null|string $ciudad_id
     */
    public function setCiudadId(?string $ciudad_id): void
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCiudad(): ?bool
    {
        return StringExtensions::toBoolean($this->include_ciudad);
    }

    /**
     * @param null|string $include_ciudad
     */
    public function setIncludeCiudad(?string $include_ciudad): void
    {
        $this->include_ciudad = $include_ciudad;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }
}