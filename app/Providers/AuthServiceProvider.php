<?php

namespace App\Providers;

use App\Domain\Candidato;
use App\Domain\ClienteRh;
use App\Domain\Factura;
use App\Domain\PuestoRh;
use App\Domain\Servicio;
use App\Domain\Usuario;
use App\Policies\BuscadorPolicy;
use App\Policies\CandidatoPolicy;
use App\Policies\ClienteRhPolicy;
use App\Policies\FacturaPolicy;
use App\Policies\ServicioPolicy;
use App\Policies\PuestoRhPolicy;
use App\Policies\UsuarioPolicy;
use App\Services\DTO\Buscador\BuscadorRequest;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Usuario::class => UsuarioPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
    }
}
