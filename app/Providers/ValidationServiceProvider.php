<?php

namespace App\Providers;

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\Usuario;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\ClassExtensions;
use App\Infrastructure\StringExtensions;
use App\Services\Validations\Implement\AuthValidator;
use App\Services\Validations\Implement\RolValidator;
use App\Services\Validations\Implement\UsuarioValidator;
use App\Services\Validations\Interfaces\IAuthValidator;
use App\Services\Validations\Interfaces\IRolValidator;
use App\Services\Validations\Interfaces\IUsuarioValidator;
use function array_first;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use function array_get;
use function array_last;
use function class_basename;

class ValidationServiceProvider extends ServiceProvider
{

    protected $messages = array();
    /* @var $unitOfWork IUnitOfWork*/
    protected $unitOfWork;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->unitOfWork = resolve(IUnitOfWork::class);
        $this->registerValidations();
        $this->registerMessages();
    }

    public function registerValidations(){
        Validator::extend('exists_bd', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $class= array_first($parameters);
            $repository = $this->unitOfWork->getEntityManager()->getRepository($class);
            $result = $repository->find($value);
            $this->unitOfWork->getEntityManager()->clear();
            return !is_null($result);
        });

        Validator::extend('validate_factura', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $validate = false;
            $class = array_first($parameters);
            $idParent = $parameters[1];
            $repositoryChild = $this->unitOfWork->getEntityManager()->getRepository($class);
            $result = $repositoryChild->find($value);
            if(StringExtensions::isNotNullOrEmpty($result)){
                if((int)$idParent === (int)$result->getServicio()->getId()){
                    $validate = true;
                }
            }
            return $validate;
        });

        Validator::extend('unique_email', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $data = $validator->getData();
            /* @var $user Usuario */
            $repository = $this->unitOfWork->getEntityManager()->getRepository(Usuario::class);
            $user = $repository->find($data["id"]);
            $sameEmail = true;
            if(!empty($user)){
                $sameEmail = $user->getEmail() === $value;
                if(!$sameEmail) {
                    $user = $repository->findBy([
                        "email" => $value
                    ]);
                    $sameEmail = empty($user);
                }
            }
            $this->unitOfWork->getEntityManager()->clear();
            return $sameEmail;
        });

        Validator::extend('entity_type', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $entityClass = array_first($parameters);
            $classAttribute = array_get($parameters, 1);
            $type = trim(array_last($parameters));
            $repository = $this->unitOfWork->getEntityManager()->getRepository($entityClass);
            $entity = $repository->find($value);
            $enum = ClassExtensions::getPrivateProperty($entity, $classAttribute);
            $this->unitOfWork->getEntityManager()->clear();
            return $enum->getValor() === $type;
        });


        Validator::extend('positive', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            return $value>=0;
        });

        Validator::extend('strict_array', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            return ArrayExtensions::isMultidimensional($value);
        });

        Validator::extend('strict_integer', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            return is_int($value);
        });

        Validator::extend('strict_numeric', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            return is_float($value) || is_int($value);
        });

        Validator::extend('float', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            return is_float($value);
        });

        Validator::extend('greater_than', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $data = $validator->getData();
            $min_field = array_first($parameters);
            $min_value = $data[$min_field];
            return $value > $min_value;
        });

        Validator::extend('less_than', function($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) {
            $min_field = array_first($parameters);
            $data = $validator->getData();
            $max_value = $data[$min_field];
            return $value < $max_value;
        });


    }

    public function registerMessages(){

        Validator::replacer('exists_bd', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $value =  array_get($validator->getData(), $attribute);
            $class=  array_first($parameters);
            $entity = StringExtensions::splitCamelCaseToWords(class_basename($class));
            return "No se encontró ningún(a) $entity con el id $value";
        });

        Validator::replacer('validate_comprobante', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El comprobante no pertenece a la factura";
        });

        Validator::replacer('positive', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El campo $attribute  debe ser un número positivo";
        });

        Validator::replacer('strict_integer', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El campo $attribute  debe ser un número entero";
        });

        Validator::replacer('strict_numeric', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El campo $attribute  debe ser un número valido";
        });

        Validator::replacer('float', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El campo $attribute  debe ser float";
        });

        Validator::replacer('strict_array', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "El campo $attribute debe ser un arreglo";
        });

        Validator::replacer('greater_than', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $mensaje = "El campo $attribute  debe ser mayor al campo $parameters[0]";
            return $mensaje;
        });

        Validator::replacer('less_than', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $mensaje = "El campo $attribute  debe ser menor al campo $parameters[0]";
            return $mensaje;
        });

        Validator::replacer('unique_email', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $mensaje = "El email ya esta en uso";
            return $mensaje;
        });

        Validator::replacer('unique', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            switch ($attribute){
                case 'email':
                    $mensaje = "El email ya esta en uso";
                    break;
                case 'rfc':
                    $mensaje = "El RFC ya esta registrado";
                    break;
                case 'clave_sat':
                    $mensaje = "La clave sat ya esta registrada";
                    break;
                case 'clave_estado':
                    $mensaje = "La clave estado ya esta registrada";
                    break;
            }
            return $mensaje;
        });

        Validator::replacer('entity_type', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $type = array_last($parameters);
            return "$attribute no es del tipo $type";
        });

        Validator::replacer('enum_type', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            $type = ClassExtensions::inverseResolveEnumClass(array_first($parameters));
            return "$attribute no es del tipo enum $type";
        });

        Validator::replacer('unique_tipo_documento_contacto', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "Ya existe un documento del mismo tipo registrado para este contacto ";
        });

        Validator::replacer('afteror_equal', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "La fecha debe ser mayor o igual al día de hoy";
        });

        Validator::replacer('panel_type', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "$attribute no es un panel";
        });

        Validator::replacer('inversor_type', function($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
            return "$attribute no es un inversor";
        });

    }



    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IAuthValidator::class,AuthValidator::class);
        $this->app->bind(IUsuarioValidator::class,UsuarioValidator::class);
        $this->app->bind(IRolValidator::class,RolValidator::class);
    }
}
