<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 1/17/19
 * Time: 12:51 PM
 */

namespace App\Policies;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Rol;
use App\Infrastructure\ProxyExtensions;
use Illuminate\Auth\Access\AuthorizationException;

class BasePolicy
{
    protected $rolRepository;

    public function __construct(IRolRepository $rolRepository)
    {
        $this->rolRepository = $rolRepository;
        $this->response = false;
        $this->error = ': no tienes permiso para la accion solicitada';
    }

    public function setErrorAuthorization($response, $error){

        if($response == false){
            throw new AuthorizationException('Autorizacion denegada'.$error);
        }
    }

    public function getRolUsuario($rolId){
        /* @var $rol Rol*/
        $rol = $this->rolRepository->get($rolId);
        ProxyExtensions::load($rol->getPermisos());
        return $rol;
    }
}