<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Infrastructure\StringExtensions;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class UsuarioPolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $usuarioRepository;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        IUsuarioRepository $usuarioRepository
    )
    {
        parent::__construct($rolRepository);
        $this->usuarioRepository = $usuarioRepository;
    }

    public function findAuthorize(Usuario $user, Request $request){
        $rolesPermitidos = ['admin-rh', 'root', 'admin-nominas', 'reclutador-rh', 'promotor-rh'];
        $rolUsuario = $this->getRolUsuario($user->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                if(StringExtensions::isNotNullOrEmpty($request->input('empresa_id'))) {
                    if($user->empresa_id == $request->input('empresa_id')){
                        $this->response = true;
                    }
                }else{
                    $this->error = ': empresa_id es requerido';
                }
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $user, $id)
    {
        $rolesPermitidos = ['admin-rh', 'root', 'admin-nominas', 'promotor-rh', 'reclutador-rh'];
        $rolUsuario = $this->getRolUsuario($user->rol_id);
        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){

                $usuario = $this->usuarioRepository->get($id);

                if(StringExtensions::isNotNullOrEmpty($usuario) && $user->empresa_id == $usuario->getEmpresa()->getId()) {
                    $this->response = true;
                }
            }
        }
        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function storeAuthorize(Usuario $user, Request $request)
    {
        $rolesPermitidos = ['admin-rh', 'root', 'admin-nominas'];
        $rolUsuario = $this->getRolUsuario($user->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                if(StringExtensions::isNotNullOrEmpty($request->input('empresa_id'))) {
                    if($user->empresa_id == $request->input('empresa_id')){
                        $this->response = true;
                    }
                }else{
                    $this->error = ': empresa_id es requerido';
                }
            }
        }
        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function updateAuthorize(Usuario $user, Request $request)
    {
        $rolesPermitidos = ['admin-rh', 'root', 'admin-nominas', 'promotor-rh', 'reclutador-rh'];
        $rolUsuario = $this->getRolUsuario($user->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                if(StringExtensions::isNotNullOrEmpty($request->input('empresa_id'))) {
                    if($user->empresa_id == $request->input('empresa_id')){
                        $usuario = $this->usuarioRepository->get($request->input('id'));
                        if(StringExtensions::isNotNullOrEmpty($usuario) && $user->empresa_id ==  $usuario->getEmpresa()->getId()) {
                            $this->response = true;
                        }
                    }
                }else{
                    $this->error = ': empresa_id es requerido';
                }
            }
        }
        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function destroyAuthorize(Usuario $user, $id)
    {
        $rolesPermitidos = ['admin-rh', 'root', 'admin-nominas'];
        $rolUsuario = $this->getRolUsuario($user->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $usuario = $this->usuarioRepository->get($id);
                if(StringExtensions::isNotNullOrEmpty($usuario) && $user->empresa_id == $usuario->getEmpresa()->getId()) {
                    $this->response = true;
                }
            }
        }
        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }
}
