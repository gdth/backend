<?php
/**
 * Created by PhpStorm.
 * Usuario: karimy chable
 * Date: 08/02/2017
 * Time: 05:43 PM
 */

namespace App\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 * @ORM\HasLifecycleCallbacks
 */
class Rol
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $display_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="Permiso", inversedBy="roles", cascade={"persist"})
     * @ORM\JoinTable(name="permiso_rol", joinColumns={@ORM\JoinColumn(name="rol_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="permiso_id", referencedColumnName="id")})
     **/
    protected $permisos;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_delete;

    /**
     * ****************************************
     *               Constructor              *
     * ****************************************
     */

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->permisos = new ArrayCollection();
    }

    /**
     * ****************************************
     *           Getters and Setters          *
     * ****************************************
     */

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * @param mixed $display_name
     */
    public function setDisplayName($display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @return mixed
     */
    public function getPermisos()
    {
        return $this->permisos;
    }

    /**
     * @return mixed
     */
    public function getIsDelete()
    {
        return $this->is_delete;
    }

    /**
     * @param mixed $is_delete
     */
    public function setIsDelete($is_delete)
    {
        $this->is_delete = $is_delete;
    }


    /**
     * @param mixed $permisos
     */
    public function setPermisos($permisos)
    {
        $this->permisos = $permisos;
    }

    public function hasPermission(Permiso $permission)
    {
        return $this->permisos->contains($permission);
    }

    public function belongsToUsuario(Usuario $usuario)
    {
        return $this->users->contains($usuario);
    }

    public function addPermission($permission)
    {
        if(is_null( $this->permisos)){
            $this->permisos = new ArrayCollection();
        }
        $this->permisos->add($permission);
    }

    public function addPermissions($permissions)
    {
        foreach ($permissions as $permission) {
            $this->addPermission($permission);
        }
    }

    public function removePermission(Permiso $permission)
    {
        if ($this->permisos->contains($permission)) {
            $this->permisos->removeElement($permission);
        }
    }

    public function removePermissions($permissions): void
    {
        foreach ($permissions as $permission) {
            $this->removePermission($permission);
        }
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new DateTime("now");
    }

}