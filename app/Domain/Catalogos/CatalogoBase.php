<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 2/01/19
 * Time: 11:59 AM
 */

namespace App\Domain;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\MappedSuperclass()
 */

class CatalogoBase
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false, length=200)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true, length=200)
     */
    protected $valor;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }
}