<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 17/12/18
 * Time: 04:27 PM
 */

namespace App\Domain;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ciudades")
 */

class CCiudad extends CatalogoBase
{
    /**
     * Many Ciudad have One Estado.
     * @ORM\ManyToOne(targetEntity="CEstado", inversedBy="ciudades")
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id")
     */
    protected $estado;

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

}