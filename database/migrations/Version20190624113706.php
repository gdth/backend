<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190624113706 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE oauth_access_tokens (id VARCHAR(100) NOT NULL, user_id INT DEFAULT NULL, client_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, scopes LONGTEXT DEFAULT NULL, revoked TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, expires_at DATETIME DEFAULT NULL, INDEX oauth_access_tokens_user_id_index (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_refresh_tokens (id VARCHAR(100) NOT NULL, access_token_id VARCHAR(100) NOT NULL, revoked TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, INDEX oauth_refresh_tokens_access_token_id_index (access_token_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_personal_access_clients (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX oauth_personal_access_clients_client_id_index (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_clients (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, secret VARCHAR(100) NOT NULL, redirect LONGTEXT NOT NULL, personal_access_client TINYINT(1) NOT NULL, password_client TINYINT(1) NOT NULL, revoked TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX oauth_clients_user_id_index (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE oauth_auth_codes (id VARCHAR(100) NOT NULL, user_id INT NOT NULL, client_id INT NOT NULL, scopes LONGTEXT DEFAULT NULL, revoked TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empresas (id INT AUTO_INCREMENT NOT NULL, estado_id INT DEFAULT NULL, ciudad_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, telefono VARCHAR(255) NOT NULL, correo VARCHAR(255) NOT NULL, direccion VARCHAR(255) NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_70DD49A59F5A440B (estado_id), INDEX IDX_70DD49A5E8608214 (ciudad_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permisos (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, display_name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_C440BE35E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuarios (id INT AUTO_INCREMENT NOT NULL, empresa_id INT DEFAULT NULL, rol_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, apellido_paterno VARCHAR(60) NOT NULL, apellido_materno VARCHAR(60) DEFAULT NULL, titulo VARCHAR(150) DEFAULT NULL, puesto VARCHAR(150) DEFAULT NULL, telefono VARCHAR(20) DEFAULT NULL, telefono_oficina VARCHAR(20) DEFAULT NULL, extension VARCHAR(255) DEFAULT NULL, celular VARCHAR(20) DEFAULT NULL, email VARCHAR(255) NOT NULL, fecha_nacimiento DATE DEFAULT NULL, password VARCHAR(255) NOT NULL, remember_token VARCHAR(100) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, activo TINYINT(1) DEFAULT NULL, primer_inicio_sesion TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_EF687F2E7927C74 (email), INDEX IDX_EF687F2521E1991 (empresa_id), INDEX IDX_EF687F24BAB96C (rol_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, display_name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, is_delete TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE permiso_rol (rol_id INT NOT NULL, permiso_id INT NOT NULL, INDEX IDX_DD501D064BAB96C (rol_id), INDEX IDX_DD501D066CEFAD37 (permiso_id), PRIMARY KEY(rol_id, permiso_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ciudades (id INT AUTO_INCREMENT NOT NULL, estado_id INT DEFAULT NULL, nombre VARCHAR(200) NOT NULL, valor VARCHAR(200) DEFAULT NULL, activo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_FF770062E892728 (valor), INDEX IDX_FF770069F5A440B (estado_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE estados (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(200) NOT NULL, valor VARCHAR(200) DEFAULT NULL, activo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_222B21282E892728 (valor), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE empresas ADD CONSTRAINT FK_70DD49A59F5A440B FOREIGN KEY (estado_id) REFERENCES estados (id)');
        $this->addSql('ALTER TABLE empresas ADD CONSTRAINT FK_70DD49A5E8608214 FOREIGN KEY (ciudad_id) REFERENCES ciudades (id)');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F2521E1991 FOREIGN KEY (empresa_id) REFERENCES empresas (id)');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F24BAB96C FOREIGN KEY (rol_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE permiso_rol ADD CONSTRAINT FK_DD501D064BAB96C FOREIGN KEY (rol_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE permiso_rol ADD CONSTRAINT FK_DD501D066CEFAD37 FOREIGN KEY (permiso_id) REFERENCES permisos (id)');
        $this->addSql('ALTER TABLE ciudades ADD CONSTRAINT FK_FF770069F5A440B FOREIGN KEY (estado_id) REFERENCES estados (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F2521E1991');
        $this->addSql('ALTER TABLE permiso_rol DROP FOREIGN KEY FK_DD501D066CEFAD37');
        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F24BAB96C');
        $this->addSql('ALTER TABLE permiso_rol DROP FOREIGN KEY FK_DD501D064BAB96C');
        $this->addSql('ALTER TABLE empresas DROP FOREIGN KEY FK_70DD49A5E8608214');
        $this->addSql('ALTER TABLE empresas DROP FOREIGN KEY FK_70DD49A59F5A440B');
        $this->addSql('ALTER TABLE ciudades DROP FOREIGN KEY FK_FF770069F5A440B');
        $this->addSql('DROP TABLE oauth_access_tokens');
        $this->addSql('DROP TABLE oauth_refresh_tokens');
        $this->addSql('DROP TABLE oauth_personal_access_clients');
        $this->addSql('DROP TABLE oauth_clients');
        $this->addSql('DROP TABLE oauth_auth_codes');
        $this->addSql('DROP TABLE empresas');
        $this->addSql('DROP TABLE permisos');
        $this->addSql('DROP TABLE usuarios');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE permiso_rol');
        $this->addSql('DROP TABLE ciudades');
        $this->addSql('DROP TABLE estados');
    }
}
