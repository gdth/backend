<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\CCiudad;
use App\Domain\CEstado;
use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    protected $unitOfWork;

    public function __construct(IUnitOfWork $unitOfWork){
        $this->unitOfWork = $unitOfWork;
    }

    public function run()
    {
        $string = file_get_contents(__DIR__."/catalogoJSON/estados-municipios.json");
        $json_a = json_decode($string, true);
        foreach ($json_a as $estadoJson => $municipios){
            $estado = new CEstado();
            $estado->setNombre($estadoJson);
            $estado->setActivo(true);
            $this->unitOfWork->insert($estado);
            foreach ($municipios as $municipio){
                $ciudad = new CCiudad();
                $ciudad->setNombre($municipio);
                $ciudad->setActivo(true);
                $ciudad->setEstado($estado);
                $this->unitOfWork->insert($ciudad);
            }
        }
    }
}