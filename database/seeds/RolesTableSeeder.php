<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\IPermisoRepository;
use App\Domain\Rol;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $unitOfWork;
    protected $permisoRepository;
    public function __construct(
        IUnitOfWork $unitOfWork,
        IPermisoRepository $permisoRepository
    ){
        $this->unitOfWork = $unitOfWork;
        $this->permisoRepository = $permisoRepository;
    }

    public function run()
    {
        //$permiso = $this->permisoRepository->get(1);
        $rol = new Rol();
        $rol->setName('root');
        $rol->setDisplayName('Super usuario');
        $rol->setDescription('Usuario root');
        $rol->setIsDelete(false);
        //$rol->addPermission($permiso);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('admin-rh');
        $rol->setDisplayName('Administrador RH');
        $rol->setDescription('Administrador de RH');
        $rol->setIsDelete(false);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('promotor-rh');
        $rol->setDisplayName('Promotor RH');
        $rol->setDescription('Promotor de RH');
        $rol->setIsDelete(false);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('reclutador-rh');
        $rol->setDisplayName('Reclutador RH');
        $rol->setIsDelete(false);
        $rol->setDescription('Reclutador de RH');
        $this->unitOfWork->insert($rol);
    }
}