<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\Permiso;
use Illuminate\Database\Seeder;

class PermisosTableSeeder extends Seeder
{
    protected $unitOfWork;
    public function __construct(
        IUnitOfWork $unitOfWork
    ){
        $this->unitOfWork = $unitOfWork;
    }

    public function run()
    {
        $permiso = new Permiso();
        $permiso->setName('crear');
        $permiso->setDisplayName('Crear');
        $permiso->setDescription('Permite crear');
        $this->unitOfWork->insert($permiso);

        $permiso = new Permiso();
        $permiso->setName('leer');
        $permiso->setDisplayName('Leer');
        $permiso->setDescription('Permite leer');
        $this->unitOfWork->insert($permiso);
    }
}