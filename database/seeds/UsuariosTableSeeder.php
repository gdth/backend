<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Usuario;
use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    protected $unitOfWork;
    protected $empresaRepository;
    protected $rolRepository;

    public function __construct(
        IUnitOfWork $unitOfWork,
        IEmpresaRepository $empresaRepository,
        IRolRepository $rolRepository
    ){
        $this->unitOfWork = $unitOfWork;
        $this->empresaRepository = $empresaRepository;
        $this->rolRepository = $rolRepository;
    }

    public function run()
    {
        $empresa = $this->empresaRepository->get(1);
        $rol = $this->rolRepository->get(2);
        $usuario = new Usuario();
        $usuario->setNombre('Jorge');
        $usuario->setApellidoPAterno('Mena');
        $usuario->setApellidoMaterno('Chim');
        $usuario->setTitulo('Ingeniero');
        $usuario->setPuesto('Desarrollador');
        $usuario->setTelefono('9675432');
        $usuario->setTelefonoOficina('9785467');
        $usuario->setExtension('111');
        $usuario->setCelular('9991414389');
        $usuario->setEmail('sistemas14@interno.com.mx');
        $usuario->setFechaNacimiento(new \DateTime('1988-06-07'));
        $usuario->setPassword(bcrypt('Pb123456*'));
        $usuario->setEmpresa($empresa);
        $usuario->setActivo(true);
        $usuario->setRol($rol);
        $usuario->setPrimerInicioSesion(true);
        $this->unitOfWork->insert($usuario);

        $rol = $this->rolRepository->get(2);
        $usuario = new Usuario();
        $usuario->setNombre('Manuel');
        $usuario->setApellidoPAterno('Euan');
        $usuario->setApellidoMaterno('Euan');
        $usuario->setTitulo('Ingeniero');
        $usuario->setPuesto('Desarrollador');
        $usuario->setTelefono('9675432');
        $usuario->setTelefonoOficina('9785467');
        $usuario->setExtension('111');
        $usuario->setCelular('9991414389');
        $usuario->setEmail('sistemas13@interno.com.mx');
        $usuario->setFechaNacimiento(new \DateTime('1990-03-01'));
        $usuario->setPassword(bcrypt('Pb123456*'));
        $usuario->setEmpresa($empresa);
        $usuario->setActivo(true);
        $usuario->setRol($rol);
        $usuario->setPrimerInicioSesion(true);
        $this->unitOfWork->insert($usuario);

        $rol = $this->rolRepository->get(4);
        $usuario = new Usuario();
        $usuario->setNombre('Oscar');
        $usuario->setApellidoPAterno('Guzman');
        $usuario->setApellidoMaterno('Euan');
        $usuario->setTitulo('Ingeniero');
        $usuario->setPuesto('Desarrollador');
        $usuario->setTelefono('9675432');
        $usuario->setTelefonoOficina('9785467');
        $usuario->setExtension('111');
        $usuario->setCelular('9991414389');
        $usuario->setEmail('sistemas1@interno.com.mx');
        $usuario->setFechaNacimiento(new \DateTime('1990-03-01'));
        $usuario->setPassword(bcrypt('Pb123456*'));
        $usuario->setEmpresa($empresa);
        $usuario->setActivo(true);
        $usuario->setRol($rol);
        $usuario->setPrimerInicioSesion(true);
        $this->unitOfWork->insert($usuario);

        $rol = $this->rolRepository->get(3);
        $usuario = new Usuario();
        $usuario->setNombre('Leonardo');
        $usuario->setApellidoPAterno('Uicab');
        $usuario->setApellidoMaterno('Uicab');
        $usuario->setTitulo('Ingeniero');
        $usuario->setPuesto('Desarrollador');
        $usuario->setTelefono('9675432');
        $usuario->setTelefonoOficina('9785467');
        $usuario->setExtension('111');
        $usuario->setCelular('9991414389');
        $usuario->setEmail('sistemas3@interno.com.mx');
        $usuario->setFechaNacimiento(new \DateTime('1990-03-01'));
        $usuario->setPassword(bcrypt('Pb123456*'));
        $usuario->setEmpresa($empresa);
        $usuario->setActivo(true);
        $usuario->setRol($rol);
        $usuario->setPrimerInicioSesion(true);
        $this->unitOfWork->insert($usuario);


    }
}