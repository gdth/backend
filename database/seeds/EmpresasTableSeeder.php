<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\ICCiudadRepository;
use App\DataAccess\Repositories\Interfaces\ICEstadoRepository;
use App\Domain\Empresa;
use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    protected $unitOfWork;
    protected $estadoRepository;
    protected $ciudadRepository;

    public function __construct(
        IUnitOfWork $unitOfWork,
        ICEstadoRepository $estadoRepository,
        ICCiudadRepository $ciudadRepository
    ){
        $this->unitOfWork = $unitOfWork;
        $this->estadoRepository = $estadoRepository;
        $this->ciudadRepository = $ciudadRepository;
    }

    public function run()
    {
        $estado = $this->estadoRepository->get(31);
        $ciudad = $this->ciudadRepository->get(2745);
        $empresa = new Empresa();
        $empresa->setNombre('PB Managment');
        $empresa->setTelefono('9284576');
        $empresa->setCorreo('info@pb.com.mx');
        $empresa->setDireccion('Calle 15 94 Colonia Mexico');
        $empresa->setEstado($estado);
        $empresa->setCiudad($ciudad);
        $empresa->setActivo(true);
        $this->unitOfWork->insert($empresa);

        /*$estado = $this->estadoRepository->get(1);
        $ciudad = $this->ciudadRepository->get(1);
        $empresa = new Empresa();
        $empresa->setNombre('AGD');
        $empresa->setTelefono('9284576');
        $empresa->setCorreo('info@pb.com.mx');
        $empresa->setDireccion('Calle 15 94 Colonia Mexico');
        $empresa->setEstado($estado);
        $empresa->setCiudad($ciudad);
        $empresa->setActivo(true);
        $this->unitOfWork->insert($empresa);*/
    }
}